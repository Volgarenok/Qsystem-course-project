#//////////////////////////////////////////////////////////////////////////////////////
#	Base make-variables: compiler, compile options, source-flag, sources directory,
#	bins directory and etc.
#//////////////////////////////////////////////////////////////////////////////////////
CC = g++
CFLAGS = -Wall -Wextra
SOURCES = -c -std=c++11
SRC_DIR = src
BIN_DIR = bin
LIB_DIR = lib
OBJ_DIR = obj

#//////////////////////////////////////////////////////////////////////////////////////
#	Project sub-directories: geometry lib, distributions lib,
#	artist and disciplines lib
#//////////////////////////////////////////////////////////////////////////////////////
GEOMETRY_DIR = geometry
DISTRIBUTIONS_DIR = distributions
ARTIST_DIR = artist
DISCIPLINES_DIR = disciplines
MAIN_DIR = main

#//////////////////////////////////////////////////////////////////////////////////////
#	Geometry curses support library (1 UT)
#	Variables 
#//////////////////////////////////////////////////////////////////////////////////////
GEOMETRY_UT = ut_geometry
GEOMETRY_LIB = libgeometry.a

_GEOMETRY_UT_OBJ = types.o geometry.o ut_geometry.o
_GEOMETRY_LIB_OBJ = types.o geometry.o
GEOMETRY_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(GEOMETRY_DIR)/%, $(_GEOMETRY_UT_OBJ))
GEOMETRY_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(GEOMETRY_DIR)/%, $(_GEOMETRY_LIB_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Distributions
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
DISTRIBUTION_LIB = libdistribution.a

_DISTRIBUTION_OBJ = normal_distribution.o uniform_distribution.o exponential_distribution.o simple_distribution.o
DISTRIBUTION_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/%, $(_DISTRIBUTION_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	SMO elements (7 UT)
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
REQUEST_UT = ut_request
DEVICE_UT = ut_device
DEVICE_UNION_UT = ut_device_union
CONSUMER_UT = ut_consumer
CONSUMER_UNION_UT = ut_consumer_union
BUFFER_UT = ut_buffer
SMO_UT = ut_smo

_REQUEST_UT_OBJ = ut_request.o
_DEVICE_UT_OBJ = ut_device.o
_DEVICE_UNION_UT_OBJ = ut_device_union.o
_CONSUMER_UT_OBJ = ut_consumer.o
_CONSUMER_UNION_UT_OBJ = ut_consumer_union.o
_BUFFER_UT_OBJ = ut_buffer.o
_SMO_UT_OBJ = ut_smo.o

_REQUEST_LIB_OBJ = request.o
_DEVICE_LIB_OBJ =
_DEVICE_UNION_LIB_OBJ = device_union.o
_CONSUMER_LIB_OBJ =
_CONSUMER_UNION_LIB_OBJ =
_BUFFER_LIB_OBJ =
_SMO_LIB_OBJ =


REQUEST_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_REQUEST_UT_OBJ))
DEVICE_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_DEVICE_UT_OBJ))
DEVICE_UNION_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_DEVICE_UNION_UT_OBJ))
CONSUMER_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_CONSUMER_UT_OBJ))
CONSUMER_UNION_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_CONSUMER_UNION_UT_OBJ))
BUFFER_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_BUFFER_UT_OBJ))
SMO_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_SMO_UT_OBJ))

REQUEST_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_REQUEST_LIB_OBJ))
DEVICE_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_DEVICE_LIB_OBJ))
DEVICE_UNION_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_DEVICE_UNION_LIB_OBJ))
CONSUMER_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_CONSUMER_LIB_OBJ))
CONSUMER_UNION_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_CONSUMER_UNION_LIB_OBJ))
BUFFER_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_BUFFER_LIB_OBJ))
SMO_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_SMO_LIB_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Disciplines (1 UT)
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
DISCIPLINES_LIB = libdisciplines.a

ORDINAL_UT = ut_ordinal
INCOMING_UT = ut_incoming
RING_UT = ut_ring
UNDERPOINTER_UT = ut_underpointer
FREE_UT = ut_free
CONSISTENT_UT = ut_consistent
CLOSING_UT = ut_closing
PRIORITY_UT = ut_priority

DISCIPLINES_UT = $(PRIORITY_UT) $(ORDINAL_UT) $(INCOMING_UT) $(RING_UT) $(UNDERPOINTER_UT) $(FREE_UT) $(CONSISTENT_UT) $(CLOSING_UT)
#DISCIPLINES_UT = $(patsubst %, $(BIN_DIR)/%\, $(_DISCIPLINES_UT))

_ORDINAL_LIB_OBJ =
_INCOMING_LIB_OBJ =
_RING_LIB_OBJ = ring.o
_UNDERPOINTER_LIB_OBJ =
_FREE_LIB_OBJ = free.o
_CONSISTENT_LIB_OBJ = consistent.o
_CLOSING_LIB_OBJ =
_PRIORITY_LIB_OBJ =

_DISCIPLINES_LIB_OBJ = $(_FREE_LIB_OBJ) $(_ORDINAL_LIB_OBJ) $(_INCOMING_LIB_OBJ) $(_RING_LIB_OBJ) $(_UNDERPOINTER_LIB_OBJ) $(_CONSISTENT_LIB_OBJ) $(_CLOSING_LIB_OBJ) $(_PRIORITY_LIB_OBJ)
DISCIPLINES_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_DISCIPLINES_LIB_OBJ))


_ORDINAL_UT_OBJ = ut_ordinal.o $(_ORDINAL_LiB_OBJ)
_INCOMING_UT_OBJ = ut_incoming.o $(_INCOMING_LIB_OBJ)
_RING_UT_OBJ = ut_ring.o $(_RING_LIB_OBJ)
_FREE_UT_OBJ = ut_free.o $(_FREE_LIB_OBJ)
_UNDERPOINTER_UT_OBJ = ut_underpointer.o $(_UNDERPOINTER_LIB_OBJ)
_CONSISTENT_UT_OBJ = ut_consistent.o $(_CONSISTENT_LIB_OBJ)
_CLOSING_UT_OBJ = ut_closing.o $(_CLOSING_LIB_OBJ)
_PRIORITY_UT_OBJ = ut_priority.o $(_PRIORITY_LIB_OBJ)

ORDINAL_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_ORDINAL_UT_OBJ))
INCOMING_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_INCOMING_UT_OBJ))
RING_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_RING_UT_OBJ))
FREE_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_FREE_UT_OBJ))
UNDERPOINTER_UT_OBJ  = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_UNDERPOINTER_UT_OBJ))
CONSISTENT_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_CONSISTENT_UT_OBJ))
CLOSING_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_CLOSING_UT_OBJ))
PRIORITY_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_PRIORITY_UT_OBJ))
ORDINAL_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_ORDINAL_LIB_OBJ))
INCOMING_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_INCOMING_LIB_OBJ))
RING_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_RING_LIB_OBJ))
FREE_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_FREE_LIB_OBJ))
UNDERPOINTER_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_UNDERPOINTER_LIB_OBJ))
CONSISTENT_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_CONSISTENT_LIB_OBJ))
CLOSING_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_CLOSING_LIB_OBJ))
PRIORITY_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(DISCIPLINES_DIR)/%, $(_PRIORITY_LIB_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Artist (2 UT)
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
ARTIST_REQUEST_UT = ut_artist_request
ARTIST_MACHINE_UT = ut_artist_machine
ARTIST_LAYOUT_UT = ut_artist_layout
ARTIST_LIB = libartist.a

_ARTIST_REQUEST_UT_OBJ = artist_request.o ut_artist_request.o
_ARTIST_MACHINE_UT_OBJ = artist_machine.o ut_artist_machine.o
_ARTIST_LAYOUT_UT_OBJ = ut_artist_layout.o artist_layout.o
_ARTIST_LAYOUT_LIB_OBJ = artist_layout.o
_ARTIST_REQUEST_LIB_OBJ = artist_request.o
_ARTIST_MACHINE_LIB_OBJ = artist_machine.o

ARTIST_REQUEST_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_REQUEST_UT_OBJ))
ARTIST_MACHINE_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_MACHINE_UT_OBJ))
ARTIST_LAYOUT_UT_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_LAYOUT_UT_OBJ))
ARTIST_REQUEST_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_REQUEST_LIB_OBJ))
ARTIST_MACHINE_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_MACHINE_LIB_OBJ))
ARTIST_LAYOUT_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/$(ARTIST_DIR)/%, $(_ARTIST_LAYOUT_LIB_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Stat module
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
STAT_MODULE_UT = ut_stat_unit

_STAT_MODULE_LIB_OBJ = stat_unit.o
_STAT_MODULE_UT_OBJ = ut_stat_unit.o $(_STAT_MODULE_LIB_OBJ)

STAT_MODULE_LIB_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_STAT_MODULE_LIB_OBJ))
STAT_MODULE_UT_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_STAT_MODULE_UT_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Main
#	Variables
#//////////////////////////////////////////////////////////////////////////////////////
MAIN = qsystem
UT_MAIN = ut_qsystem
_UT_MAIN_OBJ = ut_qsystem.o
_MAIN_OBJ = main.o qfunctions.o
MAIN_OBJ = $(patsubst %, $(OBJ_DIR)/$(MAIN_DIR)/%, $(_MAIN_OBJ))
UT_MAIN_OBJ = $(patsubst %, $(OBJ_DIR)/$(MAIN_DIR)/%, $(_UT_MAIN_OBJ))

#//////////////////////////////////////////////////////////////////////////////////////
#	Geometry curses support library
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(GEOMETRY_DIR)/%.o: $(SRC_DIR)/$(GEOMETRY_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(GEOMETRY_UT): $(GEOMETRY_UT_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses

$(LIB_DIR)/$(GEOMETRY_LIB): $(GEOMETRY_LIB_OBJ)
	ar rvs $@ $^

#//////////////////////////////////////////////////////////////////////////////////////
#	Distributions
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/%.o: $(SRC_DIR)/$(DISTRIBUTIONS_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(LIB_DIR)/$(DISTRIBUTION_LIB): $(DISTRIBUTION_OBJ)
	ar rvs $@ $^

#//////////////////////////////////////////////////////////////////////////////////////
#	Smo elements
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(REQUEST_UT): $(REQUEST_UT_OBJ) $(REQUEST_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(DEVICE_UT): $(DEVICE_UT_OBJ) $(REQUEST_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

$(DEVICE_UNION_UT): $(DEVICE_UNION_UT_OBJ) $(DEVICE_UNION_LIB_OBJ) $(DEVICE_LIB_OBJ) $(REQUEST_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

$(CONSUMER_UT): $(CONSUMER_UT_OBJ) $(REQUEST_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

$(CONSUMER_UNION_UT): $(CONSUMER_UNION_UT_OBJ) $(REQUEST_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

$(BUFFER_UT): $(BUFFER_UT_OBJ) $(REQUEST_LIB_OBJ) $(LIB_DIR)/$(DISCIPLINES_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@	

$(SMO_UT): $(SMO_UT_OBJ) $(LIB_DIR)/$(ARTIST_LIB)  $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(REQUEST_LIB_OBJ) $(DEVICE_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(CONSUMER_UNION_LIB_OBJ) $(BUFFER_LIB_OBJ) $(STAT_MODULE_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses

$(STAT_MODULE_UT): $(STAT_MODULE_UT_OBJ) $(LIB_DIR)/$(ARTIST_LIB)  $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(REQUEST_LIB_OBJ) $(DEVICE_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(CONSUMER_UNION_LIB_OBJ) $(BUFFER_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses

$(OBJ_DIR)/$(MAIN_DIR)/%.o: $(SRC_DIR)/$(MAIN_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(MAIN): $(MAIN_OBJ) $(LIB_DIR)/$(ARTIST_LIB)  $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(REQUEST_LIB_OBJ) $(DEVICE_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(CONSUMER_UNION_LIB_OBJ) $(BUFFER_LIB_OBJ) $(STAT_MODULE_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses -lboost_system -lboost_program_options

$(UT_MAIN): $(UT_MAIN_OBJ) $(LIB_DIR)/$(ARTIST_LIB)  $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(REQUEST_LIB_OBJ) $(DEVICE_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(CONSUMER_UNION_LIB_OBJ) $(BUFFER_LIB_OBJ) $(STAT_MODULE_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses -lboost_system -lboost_program_options

#//////////////////////////////////////////////////////////////////////////////////////
#	Disciplines
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(DISCIPLINES_DIR)/%.o: $(SRC_DIR)/$(DISCIPLINES_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(LIB_DIR)/$(DISCIPLINES_LIB): $(DISCIPLINES_LIB_OBJ)
	ar rvs $@ $^

$(RING_UT): $(RING_UT_OBJ) $(REQUEST_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(UNDERPOINTER_UT): $(UNDERPOINTER_UT_OBJ) $(REQUEST_LIB_OBJ) $(RING_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(FREE_UT): $(FREE_UT_OBJ) $(REQUEST_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(INCOMING_UT): $(INCOMING_UT_OBJ) $(REQUEST_LIB_OBJ) $(FREE_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(ORDINAL_UT): $(ORDINAL_UT_OBJ) $(REQUEST_LIB_OBJ) $(FREE_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(CONSISTENT_UT): $(CONSISTENT_UT_OBJ) $(REQUEST_LIB_OBJ) $(ORDINAL_LIB_OBJ) $(FREE_LIB_OBJ) $(INCOMING_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@

$(CLOSING_UT): $(CLOSING_UT_OBJ) $(CONSISTENT_LIB_OBJ) $(REQUEST_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

$(PRIORITY_UT): $(PRIORITY_UT_OBJ) $(CONSISTENT_LIB_OBJ) $(REQUEST_LIB_OBJ) $(CONSUMER_LIB_OBJ) $(LIB_DIR)/$(DISTRIBUTION_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@

#//////////////////////////////////////////////////////////////////////////////////////
#	Artist
#	Targets
#//////////////////////////////////////////////////////////////////////////////////////
$(OBJ_DIR)/$(ARTIST_DIR)/%.o: $(SRC_DIR)/$(ARTIST_DIR)/%.cpp
	$(CC) $(SOURCES) $(CFLAGS) $^ -o $@

$(ARTIST_REQUEST_UT): $(ARTIST_REQUEST_UT_OBJ) $(LIB_DIR)/$(GEOMETRY_LIB) $(REQUEST_LIB_OBJ)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses

$(ARTIST_MACHINE_UT): $(ARTIST_MACHINE_UT_OBJ) $(ARTIST_REQUEST_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(DEVICE_LIB_OBJ) $(REQUEST_LIB_OBJ) $(BUFFER_LIB_OBJ) $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses	

$(ARTIST_LAYOUT_UT): $(ARTIST_LAYOUT_UT_OBJ) $(ARTIST_MACHINE_LIB_OBJ) $(ARTIST_REQUEST_LIB_OBJ) $(DEVICE_UNION_LIB_OBJ) $(DEVICE_LIB_OBJ) $(REQUEST_LIB_OBJ) $(BUFFER_LIB_OBJ) $(LIB_DIR)/$(GEOMETRY_LIB) $(LIB_DIR)/$(DISTRIBUTION_LIB) $(LIB_DIR)/$(DISCIPLINES_LIB)
	$(CC) $^ -o $(BIN_DIR)/$@ -lpdcurses

$(LIB_DIR)/$(ARTIST_LIB): $(ARTIST_REQUEST_LIB_OBJ) $(ARTIST_MACHINE_LIB_OBJ) $(ARTIST_LAYOUT_LIB_OBJ) $(LIB_DIR)/$(GEOMETRY_LIB)	
	ar rvs $@ $^ 

#
#Fake targets
#
.PHONY: clean
.PHONY: unit_tests
.PHONY: run_auto_ut
.PHONY: run_graphic_ut

clean:
	rm -f ./*.o
	rm -f ./*.exe
	rm -f $(BIN_DIR)/*.exe
	rm -f $(LIB_DIR)/*.a
	rm -f $(OBJ_DIR)/*.o
	rm -f $(OBJ_DIR)/$(GEOMETRY_DIR)/*.o
	rm -f $(OBJ_DIR)/$(ARTIST_DIR)/*.o
	rm -f $(OBJ_DIR)/$(DISTRIBUTIONS_DIR)/*.o
	rm -f $(OBJ_DIR)/$(DISCIPLINES_DIR)/*.o
	rm -f $(OBJ_DIR)/$(MAIN_DIR)/*.o

unit_tests: $(REQUEST_UT) $(DEVICE_UT) $(DEVICE_UNION_UT) $(CONSUMER_UT) $(CONSUMER_UNION_UT) $(BUFFER_UT) $(SMO_UT) $(GEOMETRY_UT) $(ARTIST_REQUEST_UT) $(ARTIST_MACHINE_UT) $(DISCIPLINES_UT)
run_auto_ut: unit_tests
	$(BIN_DIR)/$(REQUEST_UT)
	$(BIN_DIR)/$(DEVICE_UT)
	$(BIN_DIR)/$(DEVICE_UNION_UT) 
	$(BIN_DIR)/$(CONSUMER_UT) 
	$(BIN_DIR)/$(CONSUMER_UNION_UT) 
	$(BIN_DIR)/$(BUFFER_UT) 
	$(BIN_DIR)/$(RING_UT) 
	$(BIN_DIR)/$(ORDINAL_UT)
	$(BIN_DIR)/$(INCOMING_UT)
	$(BIN_DIR)/$(UNDERPOINTER_UT)
	$(BIN_DIR)/$(FREE_UT)
	$(BIN_DIR)/$(CONSISTENT_UT)
	$(BIN_DIR)/$(CLOSING_UT)

run_graphic_ut: unit_tests
	$(BIN_DIR)/$(GEOMETRY_UT) 
	$(BIN_DIR)/$(ARTIST_REQUEST_UT)
	$(BIN_DIR)/$(ARTIST_MACHINE_UT) 
	$(BIN_DIR)/$(ARTIST_LAYOUT_UT)
	$(BIN_DIR)/$(SMO_UT)