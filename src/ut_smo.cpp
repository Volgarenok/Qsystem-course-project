#define BOOST_TEST_MODULE Test request
#include "smo.hpp"
#include "disciplines.h"
#include "distributions.h"
#include "consumer.hpp"
#include "device.hpp"
#include "main/qsystem_defines.h"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(SMO_simple_test)
{
    SMO< Buffer<>, Ordinal > smo_1(3);

    Consumer< Simple_distribution > u_consumer(1, Simple_distribution(0.8));
    Consumer< Simple_distribution > e_consumer(2, Simple_distribution(0.7));
    Consumer< Simple_distribution > n_consumer(3, Simple_distribution(0.8));

    Device< Simple_distribution > u_device(1, Simple_distribution(0.4));
    Device< Simple_distribution > e_device(2, Simple_distribution(0.7));
    Device< Simple_distribution > n_device(3, Simple_distribution(0.6));

    smo_1.push_device(u_device);
    smo_1.push_device(e_device);
    smo_1.push_device(n_device);

    smo_1.push_consumer(u_consumer);
    smo_1.push_consumer(e_consumer);
    smo_1.push_consumer(n_consumer);
    std::cout << smo_1.stepmode(10) << std::endl;
    std::cin.get();
}

BOOST_AUTO_TEST_CASE(SMO_base_test)
{
    SMO< Buffer<>, Ordinal > smo_1(3);

    Consumer< Uniform_distribution > u_consumer(1, Uniform_distribution(2.0, 5.0));
    Consumer< Exponential_distribution > e_consumer(2, Exponential_distribution());
    Consumer< Normal_distribution > n_consumer(3, Normal_distribution(5.0));

    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));

    smo_1.push_device(u_device);
    smo_1.push_device(e_device);
    //smo_1.push_device(n_device);

    smo_1.push_consumer(u_consumer);
    smo_1.push_consumer(e_consumer);
    //smo_1.push_consumer(n_consumer);
    std::cout << smo_1.stepmode(10) << std::endl;
    std::cin.get();
}

BOOST_AUTO_TEST_CASE(SMO_ring_test)
{
    SMO< Buffer< Ring, Ring, Ring >, Ring > smo_1(3);

    Consumer< Uniform_distribution > u_consumer(1, Uniform_distribution(2.0, 5.0));
    Consumer< Exponential_distribution > e_consumer(2, Exponential_distribution());
    Consumer< Normal_distribution > n_consumer(3, Normal_distribution(5.0));

    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));

    smo_1.push_device(u_device);
    smo_1.push_device(e_device);
    //smo_1.push_device(n_device);

    smo_1.push_consumer(u_consumer);
    smo_1.push_consumer(e_consumer);
    //smo_1.push_consumer(n_consumer);
    std::cout << smo_1.stepmode(10) << std::endl;
    std::cin.get();
}
