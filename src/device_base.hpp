#ifndef DEVICE_BASE_HPP
#define DEVICE_BASE_HPP
#include "request.h"
class Device_base
{
    public:
        virtual const Request & top() const = 0;
        virtual void pop() = 0;
        virtual void reset() = 0;
        virtual unsigned int amount() const = 0;
        virtual unsigned int number() const = 0;
        virtual unsigned int priority() const = 0;
        virtual ~Device_base(){}
};
#endif // DEVICE_BASE_HPP
