#ifndef DEVICE_UNION_HPP
#define DEVICE_UNION_HPP
#include "device_base.hpp"
#include "request.h"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <ostream>

class Device_union
{
    public:
        typedef std::vector< Request > dump_type;
        template < class Device_type >
        void push_device(const Device_type & device_)
        {
            if(!in_union(device_.number()))
            {
                devices.push_back(boost::make_shared< Device_type >(device_));
            }
        }

        bool in_union(const unsigned int number) const;
        void pop_device(const unsigned int number);
        bool empty() const;
        unsigned int size() const;
        const Request & top() const;
        void pop();
        void reset();
        const dump_type dump() const;
        const std::vector< boost::shared_ptr< Device_base > > devices_list() const;

    private:
        typedef Device_base base_class_type;
        typedef boost::shared_ptr< base_class_type > ptr_base_class_type;
        typedef std::vector< ptr_base_class_type > container_type;

        container_type::const_iterator find_top() const;
        container_type::iterator find_top();

        container_type devices;
};

std::ostream & operator<< (std::ostream & out, const Device_union & device_union_);
#endif // DEVICE_UNION_HPP
