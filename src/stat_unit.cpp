#include "stat_unit.h"
#include "io/io_state_guard.h"
#include <iterator>
#include <algorithm>
#include <limits>
#include <iomanip>
namespace
{
    namespace generators
    {
        struct devices_count_pairs
        {
            typedef std::pair< unsigned int, unsigned int > result_type;
            result_type operator()(const SUnit::base_device_ptr & device_ptr) const
            {
                return std::make_pair( device_ptr->number(), 0u);
            }
        };

        struct devices_time_tuples
        {
            typedef std::tuple< unsigned int, unsigned int, double > result_type;
            result_type operator()(const SUnit::base_device_ptr & device_ptr) const
            {
                return std::make_tuple(device_ptr->number(), 0u, 0.0d);
            }
        };

        struct consumers_idle_tuples
        {
            typedef std::tuple< unsigned int, double, double > result_type;
            result_type operator()(const SUnit::base_consumer_ptr & consumer_ptr) const
            {
                return std::make_tuple(consumer_ptr->number(), consumer_ptr->idle(), consumer_ptr->top().execution());
            }
        };

        struct devices_times
        {
            typedef std::pair< unsigned int, std::vector< double > > result_type;
            result_type operator()(const SUnit::base_device_ptr & device_ptr) const
            {
                return std::make_pair(device_ptr->number(), std::vector< double >(0));
            }
        };
    }

    namespace finders
    {
        class count_pair_by_device_number
        {
            public:
                typedef bool result_type;
                count_pair_by_device_number(const unsigned int device_number):
                    number(device_number)
                    {
                    }
                result_type operator()(const generators::devices_count_pairs::result_type & lhs) const
                {
                    return lhs.first == number;
                }
            private:
                count_pair_by_device_number();
                unsigned int number;
        };

        class time_tuple_by_device_number
        {
            public:
                typedef bool result_type;
                time_tuple_by_device_number(const unsigned int device_number):
                    number(device_number)
                    {
                    }
                result_type operator()(const generators::devices_time_tuples::result_type & lhs) const
                {
                    return std::get< 0 >(lhs) == number;
                }
            private:
                time_tuple_by_device_number();
                unsigned int number;
        };

        class time_pair_by_device_number
        {
            public:
                typedef bool result_type;
                time_pair_by_device_number(const unsigned int device_number):
                    number(device_number)
                    {
                    }
                result_type operator()(const generators::devices_times::result_type & lhs) const
                {
                    return lhs.first == number;
                }
            private:
                time_pair_by_device_number();
                unsigned int number;
        };
    }

    namespace converters
    {
        struct count_pairs_to_probabilities
        {
            typedef std::pair< unsigned int, double > result_type;
            result_type operator()(const generators::devices_count_pairs::result_type & executed_lhs, const generators::devices_count_pairs::result_type & refused_rhs) const
            {
                if(executed_lhs.second + refused_rhs.second == 0)
                {
                    return std::make_pair(executed_lhs.first, 0.0d);
                }
                return std::make_pair(executed_lhs.first, static_cast< double >(refused_rhs.second)/static_cast< double >(executed_lhs.second + refused_rhs.second));
            }
        };

        struct time_tuples_to_mean_residence_times
        {
            typedef std::pair< unsigned int, double > result_type;
            result_type operator()(const generators::devices_time_tuples::result_type & waiting_lhs, const generators::devices_time_tuples::result_type & executing_rhs) const
            {
                return std::make_pair(std::get< 0 >(executing_rhs), (std::get< 2 >(waiting_lhs) + std::get< 2 >(executing_rhs))/std::get< 1 >(waiting_lhs));
            }
        };

        class idle_pairs_to_loads
        {
            public:
                idle_pairs_to_loads(const double max_execution):
                    maximum(max_execution)
                    {
                    }

                typedef std::pair< unsigned int, double > result_type;
                result_type operator()(const generators::consumers_idle_tuples::result_type & lhs) const
                {
                    if(std::get< 1 >(lhs) < 0)
                    {
                        return std::make_pair(std::get< 0 >(lhs), 0.0d);
                    }
                    return std::make_pair(std::get< 0 >(lhs), (maximum - (std::get< 1 >(lhs) + maximum - std::get< 2 >(lhs)))/maximum);
                }
            private:
                idle_pairs_to_loads();
                double maximum;
        };

        class times_to_dispersion
        {
            public:
                typedef std::pair< unsigned int, double > result_type;
                result_type operator()(const generators::devices_times::result_type & lhs) const
                {
                    disperser functor = std::for_each(lhs.second.begin(), lhs.second.end(), disperser(lhs.second.size()));
                    return std::make_pair(lhs.first, functor.get());
                }
            private:
                class disperser
                {
                    public:
                        disperser(unsigned int size):
                            item_squares(size),
                            item_sum(0.0d),
                            count(0)
                            {
                            }

                        void operator()(const double value)
                        {
                            item_squares[count++] = value*value;
                            item_sum += value;
                        }

                        double get() const
                        {
                            calculate functor = std::for_each(item_squares.begin(), item_squares.end(), calculate(item_sum, count));
                            return functor.get();
                        }

                    private:
                        class calculate
                        {
                            public:
                                calculate(const double sum_, const unsigned int size_):
                                    dispersion(0.0d),
                                    mean_square(sum_/size_ * sum_/size_),
                                    size(size_)
                                    {
                                    }
                                void operator()(const double item_square)
                                {
                                    dispersion += item_square - mean_square;
                                }

                                double get() const
                                {
                                    return dispersion/(size);
                                }
                            private:
                                double dispersion;
                                double mean_square;
                                unsigned int size;
                        };

                        std::vector< double > item_squares;
                        double item_sum;
                        unsigned int count;
                };
        };
    }
}

std::ostream & operator<< (std::ostream & out, const SUnit & stat_unit)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        std::vector< std::pair< unsigned int, unsigned int > > exec_stat = stat_unit.executed();
        std::vector< std::pair< unsigned int, unsigned int > > refuse_stat = stat_unit.refused();
        std::vector< std::pair< unsigned int, double > > probability_stat = stat_unit.probability();
        std::vector< std::pair< unsigned int, double > > mean_residence_stat = stat_unit.mean_residence();
        std::vector< std::pair< unsigned int, double > > w_dispersion = stat_unit.idle_dispersion();
        std::vector< std::pair< unsigned int, double > > e_dispersion = stat_unit.execution_dispersion();
        out << " Number  Generated  Executed  Refused  Probability  Resident  Idle_dispersion  Execution_dispersion\n";
        for(unsigned int i = 0; i < exec_stat.size(); i++)
        {
            out << " " << std::setw(6) << exec_stat[i].first << " ";
            out << " " << std::setw(9) << exec_stat[i].second + refuse_stat[i].second << " ";
            out << " " << std::setw(8) << exec_stat[i].second << " ";
            out << " " << std::setw(7) << refuse_stat[i].second << " ";
            out << " " << std::setw(11) << probability_stat[i].second << " ";
            out << " " << std::setw(8) << mean_residence_stat[i].second << " ";
            out << " " << std::setw(15) << w_dispersion[i].second << " ";
            out << " " << std::setw(20) << e_dispersion[i].second << " \n";
        }
        std::vector< std::pair< unsigned int, double > > load = stat_unit.load();
        out << " Number  Load \n";
        for(unsigned int i = 0; i < load.size(); i++)
        {
            out << " " << std::setw(6) << load[i].first << " ";
            out << " " << std::setw(4) << load[i].second << "\n";
        }
        out << "\b";
    }
    return out;
}

SUnit::SUnit(const devices_container & devices_, const consumers_container & consumers_):
    waiting_times(),
    executing_times(),
    executed_requests(),
    refused_requests(),
    consumers_load(),
    max_execution(std::numeric_limits< double >::min())
    {
        std::transform(devices_.begin(), devices_.end(), std::back_inserter(executed_requests), generators::devices_count_pairs());
        refused_requests = executed_requests;

        std::transform(devices_.begin(), devices_.end(), std::back_inserter(waiting_times), generators::devices_time_tuples());
        executing_times = waiting_times;

        std::transform(consumers_.begin(), consumers_.end(), std::back_inserter(consumers_load), generators::consumers_idle_tuples());
        consumers_container::const_iterator max_execution_point = std::max_element(consumers_.begin(), consumers_.end(), [](const consumers_container::value_type & lhs, const consumers_container::value_type & rhs)
                         {
                             return lhs->top().execution() < rhs->top().execution();
                         });
        max_execution = (*max_execution_point)->top().execution();

        std::transform(devices_.begin(), devices_.end(), std::back_inserter(idle_times), generators::devices_times());
        std::transform(devices_.begin(), devices_.end(), std::back_inserter(exec_times), generators::devices_times());
    }

void SUnit::probability_handler(const Request & request)
{
    if(request.is_executed())
    {
        std::vector< std::pair< unsigned int, unsigned int > >::iterator position = std::find_if(executed_requests.begin(), executed_requests.end(), finders::count_pair_by_device_number(request.source()));
        position->second += 1u;
    }
    else
    {
        std::vector< std::pair< unsigned int, unsigned int > >::iterator position = std::find_if(refused_requests.begin(), refused_requests.end(), finders::count_pair_by_device_number(request.source()));
        position->second += 1u;
    }
}

void SUnit::time_handler(const Request & request)
{
    std::vector< std::tuple< unsigned int, unsigned int, double > >::iterator w_position = std::find_if(waiting_times.begin(), waiting_times.end(), finders::time_tuple_by_device_number(request.source()));
    std::vector< std::tuple< unsigned int, unsigned int, double > >::iterator e_position = std::find_if(executing_times.begin(), executing_times.end(), finders::time_tuple_by_device_number(request.source()));
    if(request.is_executed())
    {
        std::get< 1 >(*e_position) += 1u;
        std::get< 2 >(*e_position) += request.execution() - request.idle();
    }
    std::get< 1 >(*w_position) += 1u;
    std::get< 2 >(*w_position) += request.idle() - request.birth();
}

void SUnit::dispersion_handler(const Request & request)
{
    std::vector< std::pair< unsigned int, std::vector< double > > >::iterator e_position = std::find_if(exec_times.begin(), exec_times.end(), finders::time_pair_by_device_number(request.source()));
    std::vector< std::pair< unsigned int, std::vector< double > > >::iterator i_position = std::find_if(idle_times.begin(), idle_times.end(), finders::time_pair_by_device_number(request.source()));
    if(request.is_executed())
    {
        e_position->second.push_back(request.execution() - request.idle());
    }
    i_position->second.push_back(request.idle() - request.birth());
}

SUnit::result_type SUnit::operator()(const Request & request)
{
    if(request.valid())
    {
        probability_handler(request);
        time_handler(request);
        dispersion_handler(request);
    }
}

std::vector< std::pair< unsigned int, double > > SUnit::probability() const
{
    std::vector< std::pair< unsigned int, double > > returnable(executed_requests.size());
    std::transform(executed_requests.begin(), executed_requests.end(), refused_requests.begin(), returnable.begin(), converters::count_pairs_to_probabilities());
    return returnable;
}

std::vector< std::pair< unsigned int, double > > SUnit::mean_residence() const
{
    std::vector< std::pair< unsigned int, double > > returnable(waiting_times.size());
    std::transform(waiting_times.begin(), waiting_times.end(), executing_times.begin(), returnable.begin(), converters::time_tuples_to_mean_residence_times());
    return returnable;
}

std::vector< std::pair< unsigned int, double > > SUnit::load() const
{
    std::vector< std::pair< unsigned int, double > > returnable(consumers_load.size());
    std::transform(consumers_load.begin(), consumers_load.end(), returnable.begin(), converters::idle_pairs_to_loads(max_execution));
    return returnable;
}

std::vector< std::pair< unsigned int, unsigned int > > SUnit::executed() const
{
    return executed_requests;
}

std::vector< std::pair< unsigned int, unsigned int > > SUnit::refused() const
{
    return refused_requests;
}

std::vector< std::pair< unsigned int, double > > SUnit::idle_dispersion() const
{
    std::vector< std::pair< unsigned int, double > > idle_disp(idle_times.size());
    std::transform(idle_times.begin(), idle_times.end(), idle_disp.begin(), converters::times_to_dispersion());
    return idle_disp;
}

std::vector< std::pair< unsigned int, double > > SUnit::execution_dispersion() const
{
    std::vector< std::pair< unsigned int, double > > exec_disp(exec_times.size());
    std::transform(exec_times.begin(), exec_times.end(), exec_disp.begin(), converters::times_to_dispersion());
    return exec_disp;
}
