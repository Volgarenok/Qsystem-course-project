#ifndef Q_FUNCTIONS_H
#define Q_FUNCTION_H
#include "../stat_unit.h"
#include <algorithm>
#include <cmath>
#include <iostream>
namespace qsystem
{
    const double form_constant = std::exp(2*std::log(1.643d))/std::exp(2*std::log(0.1d));
    class meaner
    {
        public:
            meaner(const unsigned int size_):
                sum(0.0d),
                size(size_)
                {
                }

            void operator()(const std::pair< unsigned int, double > & item)
            {
                sum += item.second;
            }

            double get() const
            {
                return sum/size;
            }
        private:
            meaner();
            double sum;
            unsigned int size;
    };

    template< class System >
    SUnit auto_mode(System & sys, const unsigned int base_value)
    {
        SUnit returnable = sys.statmode(base_value);
        std::vector< std::pair< unsigned int, double > > base_probability = returnable.probability();
        meaner base_mean = std::for_each(base_probability.begin(), base_probability.end(), meaner(base_probability.size()));
        double mean_probability = base_mean.get();
        if(mean_probability == 0.0d)
        {
            std::cout << "Request amount: " << base_value << std::endl;
            return returnable;
        }
        unsigned int base_n = static_cast< unsigned int > (std::round(form_constant*(1 - mean_probability)/(mean_probability)));
        unsigned int new_n = base_n;
        double mean_probability_in;
        do
        {
            base_n = new_n;
            returnable = sys.statmode(base_n);
            std::vector< std::pair< unsigned int, double > > inner_probability = returnable.probability();
            meaner inner_mean = std::for_each(inner_probability.begin(), inner_probability.end(), meaner(inner_probability.size()));
            mean_probability_in = inner_mean.get();
            new_n = static_cast< unsigned int > (std::round(form_constant*(1 - mean_probability_in)/(mean_probability_in)));
        }
        while(std::abs(std::min(static_cast< double >(base_n)/new_n, static_cast< double >(new_n)/base_n)) >= 0.99d);
        std::cout << "Request amount: " << new_n << std::endl;
        std::cout << "Mean probability: " << mean_probability_in << std::endl;
        return returnable;
    }

    template< class System >
    SUnit step_mode(System & sys, const unsigned int amount)
    {
        SUnit returnable = sys.stepmode(amount);
        return returnable;
    }
}
#endif // Q_FUNCTIONS_H
