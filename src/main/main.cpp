#include "../smo.hpp"
#include "../consumer.hpp"
#include "../device.hpp"
#include "qfunctions.h"
#include "qsystem_defines.h"
#include <boost/program_options.hpp>
#include <string>
#include <cstdlib>
int main(int argc, char ** argv)
{
    std::string version("1.0.2");
    std::string mode;
    std::vector< unsigned int > device_priorities;
    std::vector< unsigned int > consumer_priorities;
    unsigned int buffer_size = 0;
    unsigned int request_amount = 0;

    using namespace boost::program_options;
    try
    {
        try
        {
            options_description general_desc{"General options"};
            general_desc.add_options()
            ("help,h", "Help screen")
            ("version,v","Version information")
            ("buffer_size,S", value< unsigned int >(), "Buffer size (must be specify)")
            ("mode,M", value< std::string >()->default_value("auto"), "Queue system mode")
            ("consumers,C", value< std::vector< unsigned int > >()->multitoken(), "Consumers priorities (must be specify)")
            ("devices,D", value< std::vector< unsigned int > >()->multitoken(), "Devices priorities (must be specify)")
            ("requests,R", value< unsigned int >(), "Request amount (must be specify)");

            variables_map vm;
            store(parse_command_line(argc, argv, general_desc), vm);
            notify(vm);

            if(vm.count("help"))
            {
                std::cout << general_desc << "\n";
                return 0;
            }

            if(vm.count("version"))
            {
                std::cout << "Queue system modeling. Version is: " << version << std::endl;
                return 0;
            }

            if(vm.count("buffer_size"))
            {
                buffer_size = vm["buffer_size"].as< unsigned int >();
                if(buffer_size == 0)
                {
                    std::cerr << "Buffer size must be greater zero. Use '-h' or '--help' option for help" << std::endl;
                    return 0;
                }
            }
            else
            {
                std::cerr << "Buffer size must be specified. Use '-h' or '--help' option for help" << std::endl;
                return 0;
            }
            mode = vm["mode"].as< std::string >();

            if(vm.count("consumers"))
            {
                consumer_priorities = vm["consumers"].as< std::vector< unsigned int > >();
            }
            else
            {
                std::cerr << "Consumer priorities must be specify. Use '-h' or '--help' option for help" << std::endl;
                return 0;
            }

            if(vm.count("devices"))
            {
                device_priorities = vm["devices"].as< std::vector< unsigned int > >();
            }
            else
            {
                std::cerr << "Device priorities must be specify. Use '-h' or '--help' option for help" << std::endl;
                return 0;
            }

            if(vm.count("requests"))
            {
                request_amount = vm["requests"].as< unsigned int >();
            }
            else
            {
                std::cerr << "Request amount must be specify. Use '-h' or '--help' option for help" << std::endl;
                return 0;
            }
        }
        catch(const error & ex)
        {
            std::cerr << "Wrong command line parameters. Use '-h' or '--help' option for help" << std::endl;
            return 0;
        }
    }
    catch(...)
    {
        std::cerr << "Error: unexpected command line options. Use '-h' or '--help' option for help" << std::endl;
        return 0;
    }

    SMO< qsystem::Buffer_type, qsystem::Selectable_consumer_discipline > base_qsystem(buffer_size);
    for(unsigned int i = 0; i < device_priorities.size(); i++)
    {
        base_qsystem.push_device(Device< qsystem::Device_distribution >(i, qsystem::d_distribution, device_priorities[i]));
    }

    for(unsigned int i = 0; i < consumer_priorities.size(); i++)
    {
        base_qsystem.push_consumer(Consumer< qsystem::Consumer_distribution >(i, qsystem::c_distribution, consumer_priorities[i]));
    }

    srand(time(0));
    if(mode == "auto")
    {
        SUnit stat_module = qsystem::auto_mode(base_qsystem, request_amount);
        std::cout << stat_module << std::endl;
    }
    else if(mode == "step")
    {
        SUnit stat_module = qsystem::step_mode(base_qsystem, request_amount);
        std::cout << stat_module << std::endl;
    }
    else
    {
        std::cerr << "Error: unexpected mode program option. Use -h or --help" << std::endl;
        return 0;
    }
    return 0;
}
