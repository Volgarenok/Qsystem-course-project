#ifndef QSYSTEM_DEFINES_H
#define QSYSTEM_DEFINES_H
#include "../disciplines.h"
#include "../distributions.h"
#include "../buffer.hpp"
namespace qsystem
{
    typedef Uniform_distribution Device_distribution;
    typedef Exponential_distribution Consumer_distribution;

    typedef Consistent Bufferable_discipline;
    typedef Priority Selectable_buffer_discipline;
    typedef Closing Removable_discipline;

    typedef Ring Selectable_consumer_discipline;

    typedef Buffer< Bufferable_discipline, Selectable_buffer_discipline, Removable_discipline > Buffer_type;

    Device_distribution d_distribution(1.0d, 3.0d);
    Consumer_distribution c_distribution(1.3d);
}
#endif // QSYSTEM_DEFINES_H
