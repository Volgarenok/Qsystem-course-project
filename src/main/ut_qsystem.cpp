#define BOOST_TEST_MODULE Test version qsystem
#include "../smo.hpp"
#include "../disciplines.h"
#include "../distributions.h"
#include "../consumer.hpp"
#include "../device.hpp"
#include "qsystem_defines.h"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(Version_smo_test)
{
    SMO< qsystem::Buffer_type, qsystem::Selectable_consumer_discipline > base_qsystem(3);
    for(unsigned int i = 0; i < 3; i++)
    {
        base_qsystem.push_device(Device< Simple_distribution >(i, Simple_distribution(0.1*(i + 1)), i + 1));
    }

    for(unsigned int i = 0; i < 3; i++)
    {
        base_qsystem.push_consumer(Consumer< Simple_distribution >(i, Simple_distribution(0.2*(i + 1)), i + 1));
    }
    std::cout << base_qsystem.stepmode(10) << std::endl;
    std::cin.get();
}
