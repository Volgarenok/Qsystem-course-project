#include "request.h"
#include "io/io_state_guard.h"
#include <iomanip>
Request::Request():
    source_number(0u),
    executioner_number(0u),
    request_priority(0u),
    executioner_priority(0u),
    birth_time(0.0d),
    wait_time(0.0d),
    exec_time(0.0d),
    validity(false)
    {

    }

Request::Request(const unsigned int source_, const double time_, const unsigned int priority_):
    source_number(source_),
    executioner_number(0u),
    request_priority(priority_ == 0u ? 1u : priority_),
    executioner_priority(1u),
    birth_time(time_),
    wait_time(time_),
    exec_time(time_),
    validity(true)
    {

    }

bool Request::operator==(const Request& rhs) const
{
    if(!rhs.validity)
    {
        return rhs.validity == this->validity;
    }
    return this->validity == rhs.validity && (this->source_number == rhs.source_number && this->birth_time == rhs.birth_time);
}

bool Request::operator!=(const Request& rhs) const
{
    return !this->operator==(rhs);
}

bool Request::valid() const
{
    return validity;
}

bool Request::is_executed() const
{
    return exec_time > birth_time;
}

void Request::execute(const unsigned int executioner_, const double time_, const unsigned int executioner_priority_)
{
    exec_time = time_;
    executioner_number = executioner_;
    executioner_priority = executioner_priority_;
}

void Request::await(const double time_)
{
    wait_time = time_;
}

void Request::make_invalid()
{
    validity = false;
}

void Request::force_priority()
{
    request_priority = 0u;
}

void Request::force_epriority()
{
    executioner_priority = 0u;
}

double Request::execution() const
{
    return exec_time;
}

double Request::idle() const
{
    return wait_time;
}

double Request::birth() const
{
    return birth_time;
}

unsigned int Request::priority() const
{
    return request_priority;
}

unsigned int Request::epriority() const
{
    return executioner_priority;
}

unsigned int Request::source() const
{
    return source_number;
}

unsigned int Request::executioner() const
{
    return executioner_number;
}

std::ostream & operator<< (std::ostream & out, const Request & request_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
		out << "(:S " << request_.source() << "-" << request_.priority() << " ";
        out << ":B "  << std::setprecision(4) << request_.birth() << " ";
		if(request_.idle() != request_.birth())out << ":W "  << std::setprecision(4) << request_.idle() << " ";
		if(request_.execution() != request_.birth()) out << ":E " << std::setprecision(4) << request_.execution() << " ";
		if(request_.executioner()) out << ":D " << request_.executioner() << "-" << request_.epriority() << " ";
        if(!request_.valid()) out << ":V "  << std::setw(1) << request_.valid() << " ";
        out << ")";
    }
    return out;
}

is_valid::result_type is_valid::operator()(const Request& request_) const
{
    return request_.valid();
}

is_invalid::result_type is_invalid::operator()(const Request& request_) const
{
    return !request_.valid();
}
/*
ascend_priority_compare::result_type ascend_priority_compare::operator()(const Request& lhs, const Request& rhs) const
{
    return lhs.priority() * lhs.valid() > rhs.priority() * rhs.valid();
}

descend_priority_compare::result_type descend_priority_compare::operator()(const Request& lhs, const Request& rhs) const
{
    return lhs.priority() * rhs.valid() < rhs.priority() * lhs.valid();
}

descend_birth_compare::result_type descend_birth_compare::operator()(const Request& lhs, const Request& rhs) const
{
    return lhs.birth() * lhs.valid() > rhs.birth() * rhs.valid();
}

ascend_birth_compare::result_type ascend_birth_compare::operator()(const Request& lhs, const Request& rhs) const
{
    return rhs.birth() * lhs.valid() > lhs.birth() * rhs.valid();
}*/
