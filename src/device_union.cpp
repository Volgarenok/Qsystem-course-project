#include "io/io_state_guard.h"
#include "device_union.hpp"
#include <vector>
#include <iomanip>
#include <algorithm>
#include <iterator>
std::ostream & operator<< (std::ostream & out, const Device_union & device_union_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
		out << "(:size " << std::setw(1) << device_union_.size() << " ";
		if(!device_union_.empty())
        {
            out << device_union_.top() << "\n";
            out << ":dump\n\t";
            std::vector< Request > requests = device_union_.dump();
            std::copy(requests.begin(), requests.end(), std::ostream_iterator< Request >(out, "\n\t"));
            out << "\b\b\b\b\b\b\b\b)";
        }
        else
        {
            out << ":top :dump "<< "\n)";
        }
    }
    return out;
}

const typename Device_union::dump_type Device_union::dump() const
{
    std::vector< Request > requests;
    std::transform(devices.begin(), devices.end(), std::back_inserter(requests), [](const ptr_base_class_type & ptr_device)
                   {
                       return ptr_device->top();
                   });
    return requests;
}

const std::vector< boost::shared_ptr< Device_base > > Device_union::devices_list() const
{
    return devices;
}

unsigned int Device_union::size() const
{
    return devices.size();
}

bool Device_union::empty() const
{
    return size() == 0;
}

bool Device_union::in_union(const unsigned int number) const
{
    container_type::const_iterator found = std::find_if(devices.begin(), devices.end(),  [number](const ptr_base_class_type & ptr_device)
                                                      {
                                                          return ptr_device->number() == number;
                                                      });
    return found != devices.end();
}

void Device_union::pop_device(const unsigned int number)
{
    if(in_union(number))
    {
        container_type::iterator found = std::find_if(devices.begin(), devices.end(), [number](const ptr_base_class_type & ptr_device)
                                                      {
                                                          return ptr_device->number() == number;
                                                      });
        devices.erase(found);
    }
    return;
}

void Device_union::reset()
{
    std::for_each(devices.begin(), devices.end(),[](ptr_base_class_type & ptr_device)->void
                  {
                      ptr_device->reset();
                  });
}

const Request & Device_union::top() const
{
    assert(!empty());
    container_type::const_iterator found = find_top();
    return (*found)->top();
}

void Device_union::pop()
{
    assert(!empty());
    container_type::iterator found = find_top();
    (*found)->pop();
}

Device_union::container_type::const_iterator Device_union::find_top() const
{
    return std::min_element(devices.begin(), devices.end(), [](const ptr_base_class_type & lhs, const ptr_base_class_type & rhs)
                                                      {
                                                          return lhs->top().birth() < rhs->top().birth();
                                                      });
}

Device_union::container_type::iterator Device_union::find_top()
{
    return std::min_element(devices.begin(), devices.end(), [](const ptr_base_class_type & lhs, const ptr_base_class_type & rhs)
                                                      {
                                                          return lhs->top().birth() < rhs->top().birth();
                                                      });
}
