#ifndef UNION_SUPPORT_H
#define UNION_SUPPORT_H
#include "request.h"
#include <boost/shared_ptr.hpp>
template < class Abstract_device >
class free_checker
{
    public:
        typedef bool result_type;
        typedef Abstract_device device_type;

        free_checker(const Request & request_):
            top_time(request_.idle())
            {
            }

            result_type operator()(const boost::shared_ptr< device_type > ptr_to_device) const
            {
                return (ptr_to_device->top().execution() <= top_time);
            }

    private:
        free_checker();
        double top_time;
};

class free_checker_requests
{
    public:
        typedef bool result_type;

        free_checker_requests(const Request & request_):
            top_time(request_.idle())
            {
            }

            result_type operator()(const Request & request_) const
            {
                return (request_.execution() <= top_time);
            }

    private:
        free_checker_requests();
        double top_time;
};
#endif // UNION_SUPPORT_H
