#define BOOST_TEST_MODULE Test device
#include "device.hpp"
#include "request.h"
#include "distributions.h"
#include <boost/test/included/unit_test.hpp>
#include <cstdlib>
#include <iostream>

BOOST_AUTO_TEST_CASE(test_uniform_device)
{
    Device< Uniform_distribution > test_device(1, Uniform_distribution(1.0, 3.0));
    std::cout << test_device << std::endl;
    std::cout << test_device.top() << std::endl;
}

BOOST_AUTO_TEST_CASE(test_exponential_device)
{
    Device< Exponential_distribution > test_device(1, Exponential_distribution(1.0));
    std::cout << test_device << std::endl;
    std::cout << test_device.top() << std::endl;
}

BOOST_AUTO_TEST_CASE(test_normal_device)
{
    Device< Normal_distribution > test_device(1, Normal_distribution(5.0, 2.0));
    std::cout << test_device << std::endl;
    std::cout << test_device.top() << std::endl;
    for(unsigned int i = 0; i < 10; i++)
    {
        test_device.pop();
        std::cout << test_device.top() << std::endl;
    }
}
