#define BOOST_TEST_MODULE Test consumer
#include "consumer.hpp"
#include "request.h"
#include "distributions.h"
#include <boost/test/included/unit_test.hpp>
#include <cstdlib>
#include <iostream>

BOOST_AUTO_TEST_CASE(test_uniform_consumer)
{
    Consumer< Uniform_distribution > test_consumer(1, Uniform_distribution(1.0, 3.0));
    std::cout << test_consumer;
    std::cout << test_consumer.top();

    Request request_1(1, 1.5, 2);
    Request request_2(3, 10.0, 2);
    Request request_3(2, 1.8, 2);
    request_1.await(1.2);
    request_2.await(3.4);
    request_3.await(5.3);

    test_consumer.push(request_1);
    request_1.make_invalid();
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    BOOST_TEST(test_consumer.top() != request_1);

    test_consumer.push(request_2);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    test_consumer.push(request_3);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;
}

BOOST_AUTO_TEST_CASE(test_exp_consumer)
{
    Consumer< Exponential_distribution > test_consumer(1, Exponential_distribution(2.0));
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    Request request_1(1, 1.5, 2);
    Request request_2(3, 10.0, 2);
    Request request_3(2, 1.8, 2);
    request_1.await(1.2);
    request_2.await(3.4);
    request_3.await(5.3);

    test_consumer.push(request_1);
    request_1.make_invalid();
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    BOOST_TEST(test_consumer.top() != request_1);

    test_consumer.push(request_2);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    test_consumer.push(request_3);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;
}

BOOST_AUTO_TEST_CASE(test_normal_consumer)
{
    Consumer< Normal_distribution > test_consumer(1, Normal_distribution(5.0, 2.0));
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    Request request_1(1, 1.5, 2);
    Request request_2(3, 10.0, 2);
    Request request_3(2, 1.8, 2);
    request_1.await(1.2);
    request_2.await(3.4);
    request_3.await(5.3);

    test_consumer.push(request_1);
    request_1.make_invalid();
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    BOOST_TEST(test_consumer.top() != request_1);

    test_consumer.push(request_2);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;

    test_consumer.push(request_3);
    std::cout << test_consumer << std::endl;
    std::cout << test_consumer.top() << std::endl;
}
