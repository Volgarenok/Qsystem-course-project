#ifndef SMO_HPP
#define SMO_HPP
#include "consumer_union.hpp"
#include "device_union.hpp"
#include "buffer.hpp"
#include "artist/artist_layout.hpp"
#include "stat_unit.h"
#include <cassert>
#include <vector>
#include <iostream>
#include <iterator>
#include <cstdio>
template < class Buffer_type, class Select_consumer_discipline >
class SMO
{
    public:
        SMO(unsigned int buffer_size);

        bool empty_devices() const;
        bool empty_consumers() const;
        bool in_consumer_union(const unsigned int number_) const;
        bool in_device_union(const unsigned int number_) const;

        void init_simulation();
        const SUnit stepmode(unsigned int amount);
        const SUnit statmode(unsigned int amount);

        void pop_consumer(const unsigned int number_);
        void pop_device(const unsigned int number_);


        template < class Device_type >
        void push_device(const Device_type & device_)
        {
            devices.push_device(device_);
        }

        template < class Consumer_type >
        void push_consumer(const Consumer_type & consumer_)
        {
            consumers.push_consumer(consumer_);
        }

    private:
        void make_snapshot(const unsigned int attr_1, const unsigned int attr_2, const unsigned int attr_3);
        void draw_last_snap() const;
        void draw_current_snap() const;
        void artist_prepare() const;
        void artist_end() const;
        unsigned int key_handler();

        template < class Source, class Destination >
        void extract_request(Source & source, Destination & destination, std::vector< Request > & second_memory)
        {
            Request req = destination.push(source.top());
            source.pop();
            if(req.valid())
            {
                second_memory.push_back(req);
            }
        }

        Device_union devices;
        Buffer_type buffer;
        Consumer_union< Select_consumer_discipline > consumers;

        std::vector< Request > refused;
        std::vector< Request > executed;
        std::vector< ALayout > snapshots;

        unsigned int snap_counter;
        bool force_flag;
};

template < class Buffer_type, class Select_consumer_discipline >
SMO< Buffer_type, Select_consumer_discipline >::SMO(unsigned int buffer_size):
    devices(),
    buffer(buffer_size),
    consumers(),
    refused(),
    executed(),
    snapshots(),
    snap_counter(0u),
    force_flag(false)
    {
    }

template < class Buffer_type, class Select_consumer_discipline >
const SUnit SMO< Buffer_type, Select_consumer_discipline >::statmode(const unsigned int amount)
{
    init_simulation();
    unsigned int generated = 0;
    while(generated != amount)
    {
        if(consumers.top().execution() > devices.top().birth() || buffer.empty())
        {
            buffer.await(devices.top());
            extract_request(devices, buffer, refused);
            generated++;
        }
        else
        {
            buffer.await(consumers.top());
            extract_request(buffer, consumers, executed);
        }
    }
    while(!buffer.empty())
    {
        buffer.await(consumers.top());
        extract_request(buffer, consumers, executed);
    }
    typename Consumer_union< Select_consumer_discipline >::dump_type dump = consumers.dump();
    std::copy(dump.begin(), dump.end(), std::back_inserter(executed));
    SUnit stat_module = SUnit(devices.devices_list(), consumers.consumers_list());
    stat_module = std::for_each(executed.begin(), executed.end(), stat_module);
    stat_module = std::for_each(refused.begin(), refused.end(), stat_module);
    return stat_module;
}

template < class Buffer_type, class Select_consumer_discipline >
const SUnit SMO< Buffer_type, Select_consumer_discipline >::stepmode(const unsigned int amount)
{
    init_simulation();
    artist_prepare();
    make_snapshot(COLOR_PAIR(1) | A_BOLD, COLOR_PAIR(2) | A_BOLD, A_BOLD);
    clear();
    draw_current_snap();
    while(!force_flag && key_handler());

    unsigned int generated = 0;
    //while(refused.size() + executed.size() != requests_amount)
    while(generated != amount)
    {
        if(consumers.top().execution() > devices.top().birth() || buffer.empty())
        {
            buffer.await(devices.top());
            extract_request(devices, buffer, refused);
            generated++;
        }
        else
        {
            buffer.await(consumers.top());
            extract_request(buffer, consumers, executed);
        }
        make_snapshot(COLOR_PAIR(1) | A_BOLD, COLOR_PAIR(2) | A_BOLD, A_BOLD);
        while(!force_flag && key_handler());
    }

    while(!buffer.empty())
    {
        buffer.await(consumers.top());
        extract_request(buffer, consumers, executed);
        make_snapshot(COLOR_PAIR(1) | A_BOLD, COLOR_PAIR(2) | A_BOLD, A_BOLD);
        while(!force_flag && key_handler());
    }

    bool safe_artist = true;
    while(safe_artist)
    {
        clear();
        draw_current_snap();
        int key = getch();
        if(key == 'a' || key == 'A')
        {
            if(snap_counter > 0)
            {
                snap_counter--;
            }
            continue;
        }
        if(key == 'd' || key == 'D')
        {
            if(snap_counter < snapshots.size() - 1)
            {
                snap_counter++;
            }
            continue;
        }
        safe_artist = false;
    }
    artist_end();

    typename Consumer_union< Select_consumer_discipline >::dump_type dump = consumers.dump();
    std::copy(dump.begin(), dump.end(), std::back_inserter(executed));
    SUnit stat_module = SUnit(devices.devices_list(), consumers.consumers_list());
    stat_module = std::for_each(executed.begin(), executed.end(), stat_module);
    stat_module = std::for_each(refused.begin(), refused.end(), stat_module);
    std::cout << "Executed: " << std::endl;
    std::copy(executed.begin(), executed.end(), std::ostream_iterator< Request >(std::cout, "\n"));
    std::cout << "Refused: " << std::endl;
    std::copy(refused.begin(), refused.end(), std::ostream_iterator< Request >(std::cout, "\n"));
    return stat_module;
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::init_simulation()
{
    refused.clear();
    executed.clear();
    snapshots.clear();
    consumers.reset();
    devices.reset();
    buffer = Buffer_type(buffer.size());
    force_flag = false;
    snap_counter = 0u;
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::pop_device(const unsigned int number_)
{
    devices.pop_device(number_);
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::pop_consumer(const unsigned int number_)
{
    consumers.pop_consumer(number_);
}

template < class Buffer_type, class Select_consumer_discipline >
bool SMO< Buffer_type, Select_consumer_discipline >::in_device_union(const unsigned int number_) const
{
    return devices.in_union(number_);
}

template < class Buffer_type, class Select_consumer_discipline >
bool SMO< Buffer_type, Select_consumer_discipline >::in_consumer_union(const unsigned int number_) const
{
    return consumers.in_union(number_);
}

template < class Buffer_type, class Select_consumer_discipline >
bool SMO< Buffer_type, Select_consumer_discipline >::empty_consumers() const
{
    return consumers.empty();
}

template < class Buffer_type, class Select_consumer_discipline >
bool SMO< Buffer_type, Select_consumer_discipline >::empty_devices() const
{
    return consumers.empty();
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::make_snapshot(const unsigned int attr_1, const unsigned int attr_2, const unsigned int attr_3)
{
    Request last_refused;
    if(!refused.empty())
    {
        last_refused = refused.back();
    }

    Request last_consumed;
    if(!executed.empty())
    {
        last_consumed = executed.back();
    }
    snapshots.push_back(ALayout(buffer, devices, consumers, last_refused, last_consumed, attr_1, attr_2, attr_3));
}

template < class Buffer_type, class Select_consumer_discipline >
unsigned int SMO< Buffer_type, Select_consumer_discipline >::key_handler()
{
    clear();
    draw_current_snap();

    const int key = getch();
    if (key == 'f' || key == 'F')
    {
        force_flag = true;
        return 0u;
    }

    if(key == 'a' || key == 'A')
    {
        if(snap_counter > 0u)
        {
            snap_counter--;
        }
        return 1u;
    }

    if(key == 'd' || key == 'D')
    {
        if(snap_counter < snapshots.size() - 1u)
        {
            snap_counter++;
            return 1u;
        }
        else
        {
            return 1u;
        }
    }

    if(key == 'w' || key == 'W')
    {
        snap_counter = snapshots.size() - 1;
        return 0u;
    }
    return 1u;
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::draw_last_snap() const
{
    snapshots.back().draw();
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::draw_current_snap() const
{
    snapshots[snap_counter].draw();
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::artist_prepare() const
{
    geometry::init_std_curses();
}

template < class Buffer_type, class Select_consumer_discipline >
void SMO< Buffer_type, Select_consumer_discipline >::artist_end() const
{
    clear();
    geometry::end_curses();
}
#endif // SMO_HPP
