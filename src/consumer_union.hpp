#ifndef CONSUMER_UNION_HPP
#define CONSUMER_UNION_HPP
#include "request.h"
#include "union_support.h"
#include "consumer_base.hpp"
#include "io/io_state_guard.h"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <algorithm>
#include <ostream>
#include <iterator>
#include <iomanip>
#include <cassert>

template < class Selectable >
class Consumer_union
{
    public:
        typedef std::vector< Request > dump_type;

        Consumer_union();
        template < class Consumer_type >
        void push_consumer(const Consumer_type & consumer_)
        {
            if(!in_union(consumer_.number()))
            {
                consumers.push_back(boost::make_shared< Consumer_type >(consumer_));
            }
        }

        void pop_consumer(const unsigned int number);
        void reset();

        bool in_union(const unsigned int number) const;
        bool is_free(const Request & request_) const;
        bool empty() const;
        unsigned int size() const;

        const Request & top() const;
        const Request push(const Request & request_);

        const dump_type dump() const;
        const std::vector< boost::shared_ptr< Consumer_base > > consumers_list() const;

    private:
        typedef Consumer_base base_class_type;
        typedef boost::shared_ptr< base_class_type > ptr_base_class_type;
        typedef std::vector< ptr_base_class_type > container_type;

        ptr_base_class_type find_consumer(const Request & request_) const;
        container_type::const_iterator find_top() const;

        container_type consumers;

        Selectable select_d;
};

template < class Selectable >
Consumer_union< Selectable >::Consumer_union():
    consumers(),
    select_d()
    {
    }

template < class Selectable >
const typename Consumer_union< Selectable >::dump_type Consumer_union< Selectable >::dump() const
{
    dump_type requests;
    std::transform(consumers.begin(), consumers.end(), std::back_inserter(requests), [](const ptr_base_class_type & ptr_consumer)
                   {
                       return ptr_consumer->top();
                   });
    return requests;
}

template < class Selectable >
const std::vector< boost::shared_ptr< Consumer_base > > Consumer_union< Selectable >::consumers_list() const
{
    return consumers;
}

template < class Selectable >
std::ostream & operator<< (std::ostream & out, const Consumer_union< Selectable > & consumer_union_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
		out << "(:size " << std::setw(1) << consumer_union_.size() << " ";
		if(!consumer_union_.empty())
        {
            out << consumer_union_.top() << "\n";
            out << ":dump\n\t";
            std::vector< Request > requests = consumer_union_.dump();
            std::copy(requests.begin(), requests.end(), std::ostream_iterator< Request >(out, "\n\t"));
            out << "\b\b\b\b\b\b\b\b)";
        }
        else
        {
            out << ":top :dump "<< "\n)";
        }
    }
    return out;
}

template < class Selectable >
void Consumer_union< Selectable >::pop_consumer(const unsigned int number)
{
    if(in_union(number))
    {
        container_type::iterator found = std::find_if(consumers.begin(), consumers.end(), [number](const ptr_base_class_type & ptr_consumer)
                                                      {
                                                          return ptr_consumer->number() == number;
                                                      });
        consumers.erase(found);
    }
    return;
}

template < class Selectable >
void Consumer_union< Selectable >::reset()
{
    std::for_each(consumers.begin(), consumers.end(), [](ptr_base_class_type & device_ptr)->void
                  {
                      device_ptr->reset();
                  });
}

template < class Selectable >
const Request Consumer_union< Selectable >::push(const Request & request_)
{
    assert(is_free(request_));
    Request found_request = select_d.select(dump(), free_checker_requests(request_));
    ptr_base_class_type found = find_consumer(found_request);
    /*
    ptr_base_class_type found = select_d.select(consumers, request_);
    */
    Request returnable = found->push(request_);
    return returnable;
}

template < class Selectable >
const Request & Consumer_union< Selectable >::top() const
{
    assert(!empty());
    container_type::const_iterator found = find_top();
    return (*found)->top();
}

template < class Selectable >
bool Consumer_union< Selectable >::is_free(const Request & request_) const
{
    container_type::const_iterator found = std::find_if(consumers.begin(), consumers.end(), [request_](const ptr_base_class_type & ptr_consumer)
                                                        {
                                                            return ptr_consumer->top().execution() <= request_.idle();
                                                        });
    return found != consumers.end();
}

template < class Selectable >
typename Consumer_union< Selectable >::ptr_base_class_type Consumer_union< Selectable >::find_consumer(const Request & request_) const
{
    typename container_type::const_iterator found = std::find_if(consumers.begin(), consumers.end(), [request_](const ptr_base_class_type & ptr_consumer)
                                                                 {
                                                                     return request_ == ptr_consumer->top();
                                                                 });
    assert(found != consumers.end());
    return *found;
}

template < class Selectable >
unsigned int Consumer_union< Selectable >::size() const
{
    return consumers.size();
}

template < class Selectable >
bool Consumer_union< Selectable >::empty() const
{
    return size() == 0;
}

template < class Selectable >
bool Consumer_union< Selectable >::in_union(const unsigned int number) const
{
    container_type::const_iterator found = std::find_if(consumers.begin(), consumers.end(), [number](const ptr_base_class_type & ptr_consumer)
                                                        {
                                                            return ptr_consumer->number() == number;
                                                        });
    return found != consumers.end();
}

template < class Selectable >
typename Consumer_union< Selectable >::container_type::const_iterator Consumer_union< Selectable >::find_top() const
{
    return std::min_element(consumers.begin(), consumers.end(), [](const ptr_base_class_type & lhs, const ptr_base_class_type & rhs)
                                                      {
                                                          return lhs->top().execution() < rhs->top().execution();
                                                      });
}
#endif // CONSUMER_UNION_HPP
