#define BOOST_TEST_MODULE Test buffer with underpointer disciplines
#include "../request.h"
#include "../buffer.hpp"
#include "ring.hpp"
#include "underpointer.hpp"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(test_one_size_buffer)
{
    Buffer< Ring, Ring, Underpointer > buffer(1);
    Request request(1, 2.0);
    buffer.push(request);
    buffer.pop();
    buffer.push(request);
    buffer.push(request);
    buffer.pop();
    BOOST_TEST(buffer.empty() == 1);
}

BOOST_AUTO_TEST_CASE(empty_full_test)
{
    Buffer< Ring, Ring, Underpointer > buffer(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);

    buffer.push(request_1);
    buffer.push(request_2);

    BOOST_TEST(buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(special_test_one)
{
    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);
    Request req3(3, 3.1, 3);
    Request req4(3, 3.1, 3);
    req3.make_invalid();

    std::vector< Request > init_container;
    init_container.push_back(req2);
    init_container.push_back(req3);
    init_container.push_back(req4);
    init_container.push_back(req4);
    Buffer< Ring, Ring, Underpointer > test_buffer(init_container);
    test_buffer.push(req1);
    test_buffer.push(req1);

    std::vector< Request > control_container;
    control_container.push_back(req2);
    control_container.push_back(req1);
    control_container.push_back(req1);
    control_container.push_back(req4);
    Buffer< Ring, Ring, Underpointer > control_buffer(control_container);

    BOOST_TEST(test_buffer == control_buffer);
}
