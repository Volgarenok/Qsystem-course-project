#ifndef CONSISTENT_HPP
#define CONSISTENT_HPP
#include "../request.h"
#include <algorithm>
class Consistent
{
    public:
        unsigned int pointer() const;
        template < class Container_type >
        void update(Container_type & storage) const
        {
            typename Container_type::iterator first = std::find_if(storage.begin(), storage.end(), is_invalid());
            typename Container_type::iterator second = std::find_if(first, storage.end(), is_valid());
            while(second != storage.end())
            {
                std::iter_swap(first, second);
                first = std::find_if(storage.begin(), storage.end(), is_invalid());
                second = std::find_if(first, storage.end(), is_valid());
            }
        }

        template < class Container_type >
        void emplace(Container_type & storage, const typename Container_type::value_type& request_) const
        {
            typename Container_type::iterator point = std::find_if(storage.begin(), storage.end(), is_invalid());
            *point = request_;
        }
};
#endif
