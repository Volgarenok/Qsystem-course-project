#define BOOST_TEST_MODULE Test buffer with ring disciplines
#include "../request.h"
#include "../buffer.hpp"
#include "ring.hpp"
#include <boost/test/included/unit_test.hpp>
#include <vector>
#include <list>
BOOST_AUTO_TEST_CASE(test_one_size_buffer)
{
    Buffer< Ring, Ring, Ring > buffer(1);
    Request request(1, 2.0);
    buffer.push(request);
    buffer.pop();
    buffer.push(request);
    buffer.push(request);
    buffer.pop();
    BOOST_TEST(buffer.empty() == 1);
}

BOOST_AUTO_TEST_CASE(empty_full_test)
{
    Buffer< Ring, Ring, Ring > buffer(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);

    buffer.push(request_1);
    buffer.push(request_2);

    BOOST_TEST(buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(special_test_one)
{
    Buffer< Ring, Ring, Ring, std::vector > test_buffer(3);
    Buffer< Ring, Ring, Ring, std::list > control_buffer(3);

    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);
    Request req3(1, 4.5, 1);
    Request req4(3, 2.4, 3);

    test_buffer.push(req1);
    test_buffer.push(req2);
    test_buffer.push(req3);

    control_buffer.push(req4);
    control_buffer.push(req4);
    control_buffer.push(req3);

    test_buffer.push(req4);
    test_buffer.push(req4);

    BOOST_TEST(test_buffer == control_buffer);

    test_buffer.pop();
    test_buffer.push(req1);
    std::list< Request > control_container;
    control_container.push_back(req1);
    control_container.push_back(req4);
    control_container.push_back(req3);
    Buffer< Ring, Ring, Ring, std::list > control_buffer_2(control_container);
    BOOST_TEST(test_buffer == control_buffer_2);
}

BOOST_AUTO_TEST_CASE(special_test_two)
{
    Buffer< Ring, Ring, Ring, std::vector > test_buffer(3);
    Buffer< Ring, Ring, Ring, std::list > control_buffer_1(3);
    Buffer< Ring, Ring, Ring, std::vector > control_buffer_2(3);
    Buffer< Ring, Ring, Ring, std::list > control_buffer_3(3);

    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);
    Request req3(1, 4.5, 1);
    Request req5(4, 3.8);

    test_buffer.push(req1);
    test_buffer.push(req2);
    test_buffer.push(req3);

    //First part
    test_buffer.push(req5);
    control_buffer_1.push(req5);
    control_buffer_1.push(req2);
    control_buffer_1.push(req3);
    BOOST_TEST(test_buffer == control_buffer_1);

    //Second part
    test_buffer.push(req5);
    control_buffer_2.push(req5);
    control_buffer_2.push(req5);
    control_buffer_2.push(req3);
    BOOST_TEST(test_buffer == control_buffer_2);

    //Third part
    test_buffer.pop();
    test_buffer.push(req2);
    control_buffer_3.push(req2);
    control_buffer_3.push(req5);
    control_buffer_3.push(req3);
    BOOST_TEST(test_buffer == control_buffer_3);
}
