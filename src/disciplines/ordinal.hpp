#ifndef ORDINAL_HPP
#define ORDINAL_HPP
#include "../request.h"
#include "../consumer_base.hpp"
#include "../union_support.h"
#include <boost/bind.hpp>
#include <cassert>
class Ordinal
{
    public:
        template < class Container_type >
        const typename Container_type::value_type & remove(const Container_type & storage, const unsigned int start, const typename Container_type::value_type&) const
        {
            typename Container_type::const_iterator found = storage.begin();
            std::advance(found, start);
            found = std::find_if(found, storage.end(), boost::bind(is_valid(), _1));
            assert(found != storage.end());
            return *found;
        }

        template < class Container_type >
        const typename Container_type::value_type& select(const Container_type & storage, const unsigned int = 0) const
        {
            typename Container_type::const_iterator found = std::find_if(storage.begin(), storage.end(), boost::bind(is_valid(), _1));
            assert(found != storage.end());
            return *found;
        }

        template < class Container_type >
        const typename Container_type::value_type& select(const Container_type & storage, const Request & request_) const
        {
            typename Container_type::const_iterator found = std::find_if(storage.begin(), storage.end(), boost::bind(free_checker< Consumer_base >(request_), _1));
            assert(found != storage.end());
            return *found;
        }

        template < class Container_type, class Valid_functor >
        const typename Container_type::value_type& select(const Container_type & storage, const Valid_functor & func, const unsigned int = 0) const
        {
            typename Container_type::const_iterator found = std::find_if(storage.begin(), storage.end(), boost::bind(func, _1));
            assert(found != storage.end());
            return *found;
        }

        unsigned int pointer()
        {
            return 0;
        }
};
#endif // ORDINAL_HPP
