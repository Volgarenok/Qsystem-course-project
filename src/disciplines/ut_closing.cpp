#define BOOST_TEST_MODULE Test buffer with closing discipline
#include "../request.h"
#include "../buffer.hpp"
#include "../consumer.hpp"
#include "../consumer_union.hpp"
#include "../distributions.h"
#include "closing.hpp"
#include "consistent.hpp"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(test_one_size_buffer)
{
    Buffer< Consistent, Closing, Closing > buffer(1);
    Request request(1, 2.0);
    buffer.push(request);
    buffer.pop();
    buffer.push(request);
    buffer.push(request);
    buffer.pop();
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(empty_full_test)
{
    Buffer< Consistent, Closing, Closing > buffer(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);

    buffer.push(request_1);
    buffer.push(request_2);

    BOOST_TEST(buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(special_test_one)
{
    Buffer< Consistent, Closing, Closing > test_buffer(3);

    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);
    Request req3(3, 3.1, 3);

    test_buffer.push(req1);
    test_buffer.push(req2);
    test_buffer.push(req3);
    test_buffer.push(req2);

    std::vector< Request > init_container;
    init_container.push_back(req1);
    init_container.push_back(req2);
    init_container.push_back(req2);
    Buffer< Consistent, Closing, Closing > control_buffer(init_container);

    BOOST_TEST(test_buffer == control_buffer);
    BOOST_TEST(test_buffer.top() == req2);

    test_buffer.push(req3);
    BOOST_TEST(test_buffer.top() == req3);
}

BOOST_AUTO_TEST_CASE(special_test_two)
{
    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);
    Request req3(3, 2.2, 3);
    Request req4(3, 3.1, 3);
    Request req5(4, 2.8, 1);
    Request req6(1, 2.5, 3);
    req2.make_invalid();

    std::vector< Request > init_container;
    init_container.push_back(req1);
    init_container.push_back(req2);
    init_container.push_back(req3);
    init_container.push_back(req4);
    Buffer< Consistent, Closing, Closing > test_buffer(init_container);
    test_buffer.push(req6);
    BOOST_TEST (test_buffer.top() == req4);
    test_buffer.push(req5);
    test_buffer.pop();

    std::vector< Request > control_container;
    control_container.push_back(req1);
    control_container.push_back(req3);
    control_container.push_back(req6);
    control_container.push_back(req2);
    Buffer< Consistent, Closing, Closing > control_buffer(control_container);

    BOOST_TEST(test_buffer == control_buffer);
}

BOOST_AUTO_TEST_CASE(special_test_three)
{
    Consumer< Simple_distribution > consumer_1(1, Simple_distribution(1.0));
    Consumer< Simple_distribution > consumer_2(2, Simple_distribution(0.4));
    Consumer< Simple_distribution > consumer_3(3, Simple_distribution(0.7));

    Consumer_union< Closing > consumers;

    consumers.push_consumer(consumer_1);
    consumers.push_consumer(consumer_2);
    consumers.push_consumer(consumer_3);

    Request request_1(1, 1.2, 2);
    Request request_2(3, 1.0, 1);
    Request request_3(2, 1.6, 3);
    request_1.await(2.0);
    request_2.await(2.0);
    request_3.await(2.0);

    consumers.push(request_1);
    consumers.push(request_2);
    consumers.push(request_3);

    Request request_4(1, 1.4, 2);
    request_4.await(2.8);
    request_3.execute(1, request_3.idle() + 0.7);
    Request returnable = consumers.push(request_4);
    BOOST_TEST(returnable.execution() == request_3.execution());
}
