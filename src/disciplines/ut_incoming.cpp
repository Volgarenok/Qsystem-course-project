#define BOOST_TEST_MODULE Test buffer with Incoming discipline
#include "../request.h"
#include "../buffer.hpp"
#include "ordinal.hpp"
#include "incoming.hpp"
#include "free.hpp"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(test_one_size_buffer)
{
    Buffer< Free, Ordinal, Incoming > buffer(1);
    Request request(1, 2.0);
    buffer.push(request);
    buffer.pop();
    buffer.push(request);
    buffer.push(request);
    buffer.pop();
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(empty_full_test)
{
    Buffer< Free, Ordinal, Incoming  > buffer(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);

    buffer.push(request_1);
    buffer.push(request_2);

    BOOST_TEST(buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(special_test_one)
{
    Buffer< Free, Ordinal, Incoming  > test_buffer(3);
    Buffer< Free, Ordinal, Incoming  > control_buffer(3);

    Request req1(1, 1.2, 2);
    Request req2(2, 2.3, 4);

    test_buffer.push(req1);
    test_buffer.push(req1);
    test_buffer.push(req1);
    test_buffer.push(req2);

    control_buffer.push(req1);
    control_buffer.push(req1);
    control_buffer.push(req1);

    BOOST_TEST(test_buffer == control_buffer);
}
