#ifndef CLOSING_HPP
#define CLOSING_HPP
#include "../request.h"
class Closing
{
    public:
        template < class Container_type >
        const typename Container_type::value_type & remove(const Container_type & storage, const unsigned int, const typename Container_type::value_type&) const
        {
            return *std::max_element(storage.begin(), storage.end(), [](const Request & lhs, const Request & rhs)
                                     {
                                         return lhs.birth() < rhs.birth();
                                     });
        }

        template < class Container_type, class Valid_functor >
        const typename Container_type::value_type& select(const Container_type & storage, const Valid_functor & func, const unsigned int = 0) const
        {
            const typename Container_type::const_iterator first = std::find_if(storage.begin(), storage.end(), func);
            return *std::max_element(first, storage.end(), [func](const Request & lhs, const Request & rhs)
                                     {
                                         if(lhs.is_executed() && rhs.is_executed())
                                         {
                                             return func(lhs) == func(rhs) && lhs.execution() < rhs.execution();
                                         }
                                         else
                                         {
                                             return func(lhs) == func(rhs) && lhs.birth() < rhs.birth();
                                         }
                                     });
        }
};
#endif //
