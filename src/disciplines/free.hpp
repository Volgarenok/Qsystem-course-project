#ifndef FREE_HPP
#define FREE_HPP
#include "boost/bind.hpp"
#include "../request.h"
#include <cassert>
#include <algorithm>
class Free
{
    public:
        unsigned int pointer() const;

        template < class Container_type >
        void update(Container_type &) const
        {
        }

        template < class Container_type >
        void emplace(Container_type & storage, const typename Container_type::value_type& request_) const
        {
            typename Container_type::iterator found = std::find_if(storage.begin(), storage.end(), boost::bind(is_invalid(), _1));
            if(found != storage.end())
            {
                *found = request_;
            }
            assert(found != storage.end());
        }
};
#endif //DISCIPLINE_BASE_HPP
