#ifndef RING_HPP
#define RING_HPP
#include "../request.h"
#include "../union_support.h"
#include "../consumer_base.hpp"
#include <boost/bind.hpp>
#include <algorithm>
#include <iterator>
class Ring
{
    public:
        Ring();

        template < class Container_type >
        const typename Container_type::value_type& remove(const Container_type & storage, const unsigned int, const typename Container_type::value_type&)
        {
            unsigned int old_pointer = point;
            ++point %= storage.size();
            typename Container_type::const_iterator found = storage.begin();
            std::advance(found, old_pointer);
            return *found;
        }

//        template < class Container_type >
//        const typename Container_type::value_type& select(const Container_type & storage, const unsigned int)
//        {
//            typename Container_type::const_iterator ring_point = storage.begin();
//            std::advance(ring_point, point);
//            typename Container_type::const_iterator found = std::find_if(ring_point, storage.end(), boost::bind(is_valid(), _1));
//            if(found == storage.end())
//            {
//                found = std::find_if(storage.begin(), ring_point, boost::bind(is_valid(), _1));
//            }
//            unsigned int difference = std::distance(storage.begin(), found);
//            point = ++difference % storage.size();
//            return *found;
//        }
//
//        template < class Container_type >
//        const typename Container_type::value_type& select(const Container_type & storage, const Request & request_)
//        {
//            typename Container_type::const_iterator ring_point = storage.begin();
//            std::advance(ring_point, point);
//            typename Container_type::const_iterator found = std::find_if(ring_point, storage.end(), boost::bind(free_checker< Consumer_base >(request_), _1));
//            if(found == storage.end())
//            {
//                found = std::find_if(storage.begin(), ring_point, boost::bind(free_checker< Consumer_base >(request_), _1));
//            }
//            unsigned int difference = std::distance(storage.begin(), found);
//            point = ++difference % storage.size();
//            return *found;
//        }

        template < class Container_type, class Valid_functor >
        const typename Container_type::value_type& select(const Container_type & storage, const Valid_functor & func, const unsigned int = 0) const
        {
            typename Container_type::const_iterator ring_point = storage.begin();
            std::advance(ring_point, point);
            typename Container_type::const_iterator found = std::find_if(ring_point, storage.end(), boost::bind(func, _1));
            if(found == storage.end())
            {
                found = std::find_if(storage.begin(), ring_point, boost::bind(func, _1));
            }
            unsigned int difference = std::distance(storage.begin(), found);
            //point = ++difference % storage.size();
            return *found;
        }

        template < class Container_type, class Valid_functor >
        const typename Container_type::value_type& select(const Container_type & storage, const Valid_functor & func, const unsigned int = 0)
        {
            typename Container_type::const_iterator ring_point = storage.begin();
            std::advance(ring_point, point);
            typename Container_type::const_iterator found = std::find_if(ring_point, storage.end(), boost::bind(func, _1));
            if(found == storage.end())
            {
                found = std::find_if(storage.begin(), ring_point, boost::bind(func, _1));
            }
            unsigned int difference = std::distance(storage.begin(), found);
            point = ++difference % storage.size();
            return *found;
        }

        unsigned int pointer() const;

        template < class Container_type >
        void update(Container_type &) const
        {

        }

        template < class Container_type >
        void emplace(Container_type & storage, const typename Container_type::value_type& request_)
        {
            typename Container_type::iterator ring_point = storage.begin();
            std::advance(ring_point, point);
            typename Container_type::iterator found = std::find_if(ring_point, storage.end(), boost::bind(is_invalid(), _1));
            if(found == storage.end())
            {
                found = std::find_if(storage.begin(), ring_point, boost::bind(is_invalid(), _1));
            }
            unsigned int difference = std::distance(storage.begin(), found);
            point = ++difference % storage.size();
            *found = request_;
        }

    private:
        unsigned int point;
};
#endif // RING_H
