#ifndef UNDERPOINTER_HPP
#define UNDERPOINTER_HPP
#include <cassert>
#include <iterator>
class Underpointer
{
    public:
        template < class Container_type >
        const typename Container_type::value_type & remove(const Container_type & storage, const unsigned int pointer, const typename Container_type::value_type&) const
        {
            typename Container_type::const_iterator found = storage.begin();
            assert(pointer < storage.size());
            std::advance(found, pointer);
            return *found;
        }
};
#endif // COMING_HPP
