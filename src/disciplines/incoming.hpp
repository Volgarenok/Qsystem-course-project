#ifndef INCOMING_HPP
#define INCOMING_HPP
#include <cassert>
class Incoming
{
    public:
        template < class Container_type >
        const typename Container_type::value_type & remove(const Container_type &, const unsigned int, const typename Container_type::value_type& request_) const
        {
            return request_;
        }
};
#endif // INCOMING_HPP
