#define BOOST_TEST_MODULE Test buffer with base disciplines
#include "buffer.hpp"
#include "request.h"
#include "disciplines.h"
#include <boost/test/included/unit_test.hpp>
#include <vector>
#include <list>
BOOST_AUTO_TEST_CASE(test_zero_size_buffer)
{
    Buffer<> buffer(0);
    Request request(1, 2.0);
    buffer.push(request);
}


BOOST_AUTO_TEST_CASE(test_one_size_buffer)
{
    Buffer<> buffer(1);
    Request request(1, 2.0);
    std::cout << buffer << std::endl;
    buffer.push(request);
    std::cout << buffer << std::endl;
    BOOST_TEST(!buffer.empty());
}

BOOST_AUTO_TEST_CASE(empty_full_test)
{
    Buffer<> buffer(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);

    buffer.push(request_1);
    buffer.push(request_2);

    BOOST_TEST(buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(!buffer.empty());

    buffer.pop();

    BOOST_TEST(!buffer.full());
    BOOST_TEST(buffer.empty());
}

BOOST_AUTO_TEST_CASE(remove_test)
{
    Buffer< Free, Ordinal, Incoming, std::list > buffer_1(2);
    Buffer< Free, Ordinal, Incoming, std::vector > buffer_2(2);
    Request request_1(1, 2.0);
    Request request_2(1, 3.0);
    Request request_3(1, 4.0);

    BOOST_TEST(buffer_1 == buffer_2);
    buffer_1.push(request_1);
    buffer_1.push(request_2);
    buffer_1.push(request_3);

    std::vector< Request > init_container;
    init_container.push_back(request_3);
    init_container.push_back(request_2);
    Buffer<> buffer_3(init_container);

    BOOST_TEST(buffer_1 != buffer_3);
    buffer_3.pop();
    buffer_3.push(request_1);

    BOOST_TEST(buffer_1 == buffer_3);
    BOOST_TEST(buffer_1.top() == buffer_3.top());
}

