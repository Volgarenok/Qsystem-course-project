#ifndef CONSUMER_HPP
#define CONSUMER_HPP
#include "request.h"
#include "io/io_state_guard.h"
#include "consumer_base.hpp"
#include <iomanip>

template < typename Time_distribution >
class Consumer: public Consumer_base
{
    public:
        Consumer(const unsigned int number_, const Time_distribution distribution_, const unsigned int priority_ = 0);
        virtual ~Consumer();

        const Request & top() const;
        const Request push(const Request & request_);
        void reset();

        unsigned int amount() const;
        unsigned int number() const;
        unsigned int priority() const;
        Time_distribution distribution() const;
        double idle() const;

    private:
        Consumer();

        Time_distribution t_distribution;

        Request current;

        double idle_time;

        unsigned int consumer_priority;
        unsigned int amount_requests;
        unsigned int consumer_number;
};

template < typename Time_distribution >
std::ostream & operator<< (std::ostream & out, const Consumer< Time_distribution > & consumer_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
		out << "(:priority " << std::setw(1) << consumer_.priority() << " ";
		out << ":amount "  << std::setw(3) << consumer_.amount() << " ";
        out << ":number "  << std::setw(1) << consumer_.number() <<  " ";
        out << ":distribution "  << std::setw(3) << consumer_.distribution() << ")";
    }
    return out;
}


template < typename Time_distribution >
Consumer< Time_distribution >::~Consumer()
{

}

template < typename Time_distribution >
Consumer< Time_distribution >::Consumer(const unsigned int number_, const Time_distribution distribution_, const unsigned int priority_):
    t_distribution(distribution_),
    current(),
    idle_time(0.0),
    consumer_priority(priority_),
    amount_requests(0),
    consumer_number(number_)
    {
    }


template < typename Time_distribution >
const Request & Consumer< Time_distribution >::top() const
{
    return current;
}

template < typename Time_distribution >
const Request Consumer< Time_distribution >::push(const Request & request_)
{
    idle_time +=  request_.idle() - current.execution();
    Request old_request = current;
    current = request_;
    current.execute(consumer_number, request_.idle() + t_distribution.get(), consumer_priority);
    amount_requests++;
    return old_request;
}

template < typename Time_distribution >
void Consumer< Time_distribution >::reset()
{
    current = Request();
    idle_time = 0.0d;
    amount_requests = 0u;
}

template < typename Time_distribution >
double Consumer< Time_distribution >::idle() const
{
    if(amount_requests == 0)
    {
        return -1.0d;
    }
    return idle_time;
}

template < typename Time_distribution >
unsigned int Consumer< Time_distribution >::priority() const
{
    return consumer_priority;
}

template < typename Time_distribution >
Time_distribution Consumer< Time_distribution >::distribution() const
{
    return t_distribution;
}

template < typename Time_distribution >
unsigned int Consumer< Time_distribution >::amount() const
{
    return amount_requests;
}

template < typename Time_distribution >
unsigned int Consumer< Time_distribution >::number() const
{
    return consumer_number;
}
#endif // CONSUMER_HPP
