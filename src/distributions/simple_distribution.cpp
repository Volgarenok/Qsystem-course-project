#include "simple_distribution.h"
#include "../io/io_state_guard.h"

Simple_distribution::Simple_distribution(const double step_):
    step_distribution(step_)
    {
    }

double Simple_distribution::get() const
{
    return step_distribution;
}

double Simple_distribution::step() const
{
    return step_distribution;
}


std::ostream & operator<< (std::ostream & out, const Simple_distribution & distribution)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:type " << "simple ";
        out << ":step " << distribution.step() << ")";
    }
    return out;
}
