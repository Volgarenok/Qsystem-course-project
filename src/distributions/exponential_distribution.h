#ifndef EXPONENTIAL_DISTRIBUTION_H
#define EXPONENTIAL_DISTRIBUTION_H
#include <ostream>
class Exponential_distribution
{
    public:
        Exponential_distribution(const double lambda_ = 1.0);
        double get() const;
        double lambda() const;

    private:
        double lambda_amount;
};

std::ostream & operator<< (std::ostream & out, const Exponential_distribution & distribution);
#endif // EXPONENTIAL_DISTRIBUTION_H
