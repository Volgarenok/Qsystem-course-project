#ifndef SIMPLE_DISTRIBUTION
#define SIMPLE_DISTRIBUTION
#include <ostream>
class Simple_distribution
{
    public:
        Simple_distribution(const double step_ = 0.5d);
        double get() const;
        double step() const;

    private:
        double step_distribution;
};

std::ostream & operator<< (std::ostream & out, const Simple_distribution & distribution);
#endif // SIMPLE_DISTRIBUTION
