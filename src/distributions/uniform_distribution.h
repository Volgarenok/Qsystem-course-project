#ifndef UNIFORM_DISTRIBUTION_H
#define UNIFORM_DISTRIBUTION_H
#include <ostream>
class Uniform_distribution
{
    public:
        Uniform_distribution(const double left_border_, const double right_border_);
        double get() const;
        double left() const;
        double right() const;

    private:
        double left_border;
        double right_border;
};

std::ostream & operator<< (std::ostream & out, const Uniform_distribution & distribution);
#endif // UNIFROM_DISTRIBUTION_H
