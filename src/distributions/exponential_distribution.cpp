#include "exponential_distribution.h"
#include "../io/io_state_guard.h"
#include <cmath>
#include <cstdlib>

Exponential_distribution::Exponential_distribution(const double lambda_):
    lambda_amount(lambda_)
    {

    }

double Exponential_distribution::get() const
{
    unsigned int random = rand();
    while(random == 0u || random == RAND_MAX)
    {
        random = rand();
    }
    double returnable = -1/lambda_amount*log(static_cast< double >(random)/RAND_MAX);
    return returnable;
}

double Exponential_distribution::lambda() const
{
    return lambda_amount;
}

std::ostream & operator<< (std::ostream & out, const Exponential_distribution & distribution)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:type " << "exponential ";
        out << ":lambda " << distribution.lambda() << ")";
    }
    return out;
}

