#include "uniform_distribution.h"
#include "../io/io_state_guard.h"
#include <cstdlib>
Uniform_distribution::Uniform_distribution(const double left_border_, const double right_border_):
    left_border(left_border_),
    right_border(right_border_)
    {

    }

double Uniform_distribution::get() const
{
    unsigned int random = rand();
    while(random == 0u || random == RAND_MAX)
    {
        random = rand();
    }
    double returnable = left_border + (right_border - left_border) * static_cast< double >(random)/RAND_MAX;
    return returnable;
}

double Uniform_distribution::left() const
{
    return left_border;
}

double Uniform_distribution::right() const
{
    return right_border;
}

std::ostream & operator<< (std::ostream & out, const Uniform_distribution & distribution)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:type " << "uniform ";
        out << ":left " << distribution.left() << " ";
        out << ":right " << distribution.right() << ")";
    }
    return out;
}
