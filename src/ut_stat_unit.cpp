#define BOOST_TEST_MODULE Test stat unit
#include "smo.hpp"
#include "disciplines.h"
#include "distributions.h"
#include "consumer.hpp"
#include "device.hpp"
#include <iostream>
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(SMO_simple_test)
{
    SMO< Buffer<>, Ordinal > smo_1(3);

    Consumer< Simple_distribution > consumer_1(1, Simple_distribution(1.3));
    Consumer< Simple_distribution > consumer_2(2, Simple_distribution(1.7));

    Device< Simple_distribution > device_1(1, Simple_distribution(0.3));
    Device< Simple_distribution > device_2(2, Simple_distribution(0.7));
    Device< Simple_distribution > device_3(2, Simple_distribution(0.6));

    smo_1.push_device(device_1);
    smo_1.push_device(device_2);
    smo_1.push_device(device_3);

    smo_1.push_consumer(consumer_1);
    smo_1.push_consumer(consumer_2);

    SUnit stat = smo_1.stepmode(10);
    std::cout << stat << std::endl;
}
