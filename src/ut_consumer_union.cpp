#define BOOST_TEST_MODULE Test consumer union
#include "consumer.hpp"
#include "consumer_union.hpp"
#include "request.h"
#include "distributions.h"
#include "disciplines.h"
#include <boost/test/included/unit_test.hpp>
#include <iostream>

BOOST_AUTO_TEST_CASE(test_consumer)
{
    Consumer< Uniform_distribution > u_consumer(1, Uniform_distribution(1.0, 3.0));
    Consumer< Exponential_distribution > e_consumer(2, Exponential_distribution());
    Consumer< Normal_distribution > n_consumer(3, Normal_distribution(5.0));

    Consumer_union< Ordinal > consumers;

    consumers.push_consumer(u_consumer);
    consumers.push_consumer(e_consumer);
    consumers.push_consumer(n_consumer);

    std::cout << consumers << std::endl;
    std::cout << u_consumer.top() << std::endl << e_consumer.top() << std::endl << n_consumer.top() << std::endl;

    Request request_1(1, 1.2, 2);
    Request request_2(3, 1.0, 1);
    Request request_3(2, 1.6, 3);
    Request request_4(1, 0.2, 5);
    Request request_5(4, 1.8, 5);
    request_1.await(0.2);
    request_2.await(0.5);
    request_3.await(0.3);
    request_4.await(2.0);
    request_5.await(1.6);

    consumers.push(request_1);
    std::cout << consumers << std::endl;

    consumers.push(request_2);
    std::cout << consumers << std::endl;

    consumers.push(request_3);
    std::cout << consumers << std::endl;

    consumers.push(request_4);
    std::cout << consumers << std::endl;

    consumers.push(request_5);
    std::cout << consumers << std::endl;
}
