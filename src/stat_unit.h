#ifndef STAT_UNIT_H
#define STAT_UNIT_H
#include "request.h"
#include "device_base.hpp"
#include "consumer_base.hpp"
#include <boost/shared_ptr.hpp>
#include <vector>
#include <utility>
#include <tuple>
class SUnit
{
    public:
        typedef Device_base base_device;
        typedef Consumer_base base_consumer;
        typedef boost::shared_ptr< base_device > base_device_ptr;
        typedef boost::shared_ptr< base_consumer > base_consumer_ptr;
        typedef std::vector< base_device_ptr > devices_container;
        typedef std::vector< base_consumer_ptr > consumers_container;

        SUnit(const devices_container & devices, const consumers_container & consumers);
        std::vector< std::pair< unsigned int, unsigned int > > executed() const;
        std::vector< std::pair< unsigned int, unsigned int > > refused() const;
        std::vector< std::pair< unsigned int, double > > probability() const;
        std::vector< std::pair< unsigned int, double > > mean_residence() const;
        std::vector< std::pair< unsigned int, double > > load() const;
        std::vector< std::pair< unsigned int, double > > idle_dispersion() const;
        std::vector< std::pair< unsigned int, double > > execution_dispersion() const;

        typedef void result_type;
        void operator()(const Request & request);
    private:
        SUnit();
        void probability_handler(const Request & request);
        void time_handler(const Request & request);
        void dispersion_handler(const Request & request);

        std::vector< std::tuple< unsigned int, unsigned int, double > > waiting_times;
        std::vector< std::tuple< unsigned int, unsigned int, double > > executing_times;

        std::vector< std::pair< unsigned int, unsigned int > > executed_requests;
        std::vector< std::pair< unsigned int, unsigned int > > refused_requests;

        std::vector< std::tuple< unsigned int, double, double > > consumers_load;
        double max_execution;

        std::vector< std::pair< unsigned int, std::vector< double > > > idle_times;
        std::vector< std::pair< unsigned int, std::vector< double > > > exec_times;
};
std::ostream & operator<< (std::ostream & out, const SUnit & stat_unit);
#endif // STAT_UNIT
