#ifndef TYPES_HPP
#define TYPES_HPP
#include <curses.h>
#include <string>
namespace geometry
{
    typedef char char_type;
    typedef std::basic_string< char_type > string_type;

    void init_std_curses();
    void end_curses();

    class attribute_guard
    {
        public:
            attribute_guard(const unsigned int attribute_);
            ~attribute_guard();

        private:
			attribute_guard(const attribute_guard &);
			attribute_guard & operator= (const attribute_guard &);
			attribute_guard();
            unsigned int attribute;
    };

    struct point
    {
        point(const unsigned int x_ = 0, const unsigned int y_ = 0);
        point move(const unsigned int dx, const unsigned int dy) const;

        unsigned int x;
        unsigned int y;
    };

    struct tip
    {
        tip(const char_type left_top_, const char_type right_bot_);

        char_type left_top;
        char_type right_bot;
    };

    struct border
    {
        border(const char_type left_top_, const char_type top_, const char_type right_top_, const char_type right_, const char_type right_bot_, const char_type bot_, const char_type left_bot_, const char_type left_);

        char_type left_top;
        char_type top;
        char_type right_top;
        char_type right;
        char_type right_bot;
        char_type bot;
        char_type left_bot;
        char_type left;
    };

    const point origin = point(0, 0);

    namespace chars
    {
        const char_type single_h = '\xC4';
        const char_type single_v = '\xB3';
        const char_type single_left = '\xC3';
        const char_type single_right = '\xB4';
        const char_type single_top = '\xC2';
        const char_type single_bot = '\xC1';
        const char_type single_left_top = '\xDA';
        const char_type single_right_top = '\xBF';
        const char_type single_right_bot = '\xD9';
        const char_type single_left_bot = '\xC0';


        const char_type twin_h = '\xCD';
        const char_type twin_v = '\xBA';
        const char_type twin_left = '\xCC';
        const char_type twin_right = '\xB9';
        const char_type twin_top = '\xCB';
        const char_type twin_bot = '\xCA';
        const char_type twin_left_top = '\xC9';
        const char_type twin_right_top = '\xBB';
        const char_type twin_right_bot = '\xBC';
        const char_type twin_left_bot = '\xC8';
    }

    namespace tips
    {
        const tip twin_h = tip(chars::twin_left, chars::twin_right);
        const tip twin_v = tip(chars::twin_top, chars::twin_bot);
        const tip single_h = tip(chars::single_left, chars::single_right);
        const tip single_v = tip(chars::single_top, chars::single_bot);
    }

    namespace borders
    {
        const border twin_square = border(chars::twin_left_top, chars::twin_h, chars::twin_right_top, chars::twin_v, chars::twin_right_bot, chars::twin_h, chars::twin_left_bot, chars::twin_v);
        const border single_square = border(chars::single_left_top, chars::single_h, chars::single_right_top, chars::single_v, chars::single_right_bot, chars::single_h, chars::single_left_bot, chars::single_v);
    }
}
#endif // TYPES_HPP

