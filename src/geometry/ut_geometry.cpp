#define BOOST_TEST_MODULE Test pdcurses plugin
#include "geometry.hpp"
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_CASE(simple_test)
{
    using namespace geometry;
    initscr();
    start_color();
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);
    init_pair(2, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_RED, COLOR_BLACK);

    line_h(origin, point(10, 10), chars::single_h, COLOR_PAIR(1) | A_BOLD);
    line_v(origin.move(0, 2), point(0, 10), chars::twin_v, COLOR_PAIR(3) | A_BOLD);
    named_line_h("Yup, that tasted purple!..", point(4, 4), point(50, 4), chars::twin_h, COLOR_PAIR(2)| A_BOLD);
    snippet_h(point(2, 10), point(10, 10), tips::twin_h, chars::twin_h, COLOR_PAIR(3)| A_BOLD);
    snippet_v(point(1, 5), point(1, 30), tips::single_v, chars::single_v, COLOR_PAIR(2) | A_BOLD);
    rectangle(point(10, 10), point(12, 11), borders::twin_square, COLOR_PAIR(1)| A_BOLD);
    named_rectangle("Rise and shine!", point(10, 15), point(50, 30), borders::single_square, COLOR_PAIR(1) | A_BOLD);
    named_rectangle("Rise and shine!", point(12, 17), point(12, 18), borders::single_square, COLOR_PAIR(1) | A_BOLD);
    label("Welcome to the league of Draven!", point(14, 20), point(48, 22), borders::twin_square, COLOR_PAIR(2) | A_BOLD);
    named_label("Defender", "Defend Dread Masters! No matter the cost!", point(14, 25), point(48, 29), borders::single_square, COLOR_PAIR(4) | A_BOLD);
    getch();
    endwin();
}

