#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP
#include "types.hpp"
namespace geometry
{
    void print(const string_type & str, const unsigned int x, const unsigned int y);

    const point & left_point(const point & pt_1, const point & pt_2);
    const point & right_point(const point & pt_1, const point & pt_2);
    const point & top_point(const point & pt_1, const point & pt_2);
    const point & bot_point(const point & pt_1, const point & pt_2);
    unsigned int count_width(const point & pt_1, const point & pt_2);
    unsigned int count_height(const point & pt_1, const point & pt_2);

    void line_h(const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute);
    void line_v(const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute);

    void named_line_h(const char_type * name, const point & pt_1, const point & pt_2, const char_type ch, const unsigned int attribute);

    void snippet_h(const point & pt_1, const point & pt_2, const tip & tp, const char_type ch, const unsigned int attribute);
    void snippet_v(const point & pt_1, const point & pt_2, const tip & tp, const char_type ch, const unsigned int attribute);

    void rectangle(const point & left_top, const point & right_bot, const border & br, const unsigned int attribute);
    void named_rectangle(const char_type * name, const point & left_top, const point & right_bot, const border & br, const unsigned int attribute);

    void label(const char_type * label, const point & left_top, const point & right_bot, const border & br, const unsigned int attribute);
    void named_label(const char_type * name, const char_type * label, const point & left_bot, const point & right_top, const border & br, const unsigned int attribute);
}
#endif // GEOMETRY_HPP
