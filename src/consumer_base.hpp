#ifndef CONSUMER_BASE_HPP
#define CONSUMER_BASE_HPP
#include "request.h"
class Consumer_base
{
    public:
        virtual const Request & top() const = 0;
        virtual const Request push(const Request & request_) = 0;
        virtual void reset() = 0;
        virtual unsigned int amount() const = 0;
        virtual unsigned int number() const = 0;
        virtual unsigned int priority() const = 0;
        virtual double idle() const = 0;
        virtual ~Consumer_base(){}
};
#endif // CONSUMER_BASE_HPP
