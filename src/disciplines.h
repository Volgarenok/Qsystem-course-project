#ifndef DISCIPLINES_H
#define DISCIPLINES_H
#include "disciplines/ring.hpp"
#include "disciplines/incoming.hpp"
#include "disciplines/ordinal.hpp"
#include "disciplines/free.hpp"
#include "disciplines/underpointer.hpp"
#include "disciplines/consistent.hpp"
#include "disciplines/priority.hpp"
#include "disciplines/closing.hpp"
#endif // DISCIPLINES_H
