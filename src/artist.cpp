#include "artist.hpp"
#include "geometry/geometry.hpp"
#include <curses.h>
#include <string>
#include <ostream>
namespace pdc
{
    void print(const std::string & str, const unsigned int x, const unsigned int y)
    {
        mvprintw(y, x, str.c_str());
    }

    class simple_printer
    {
        public:
            simple_printer(const unsigned int x_, const unsigned int y_):
                x(x_),
                y(y_)
                {
                }

            void reset_xy(const unsigned int x_, const unsigned int y_)
            {
                x = x_;
                y = y_;
            }


            void operator()(const std::string & str)
            {
                print(str, x, y++);
            }
        private:
            unsigned int x;
            unsigned int y;
    };
}

void Artist::calculation()
{
    length_counter functor;
    functor = std::for_each(buffer_requests.begin(), buffer_requests.end(), functor);
    parametres.buffer_request_length = functor.glength();
    functor.reset();

    functor = std::for_each(devices_requests.begin(), devices_requests.end(), functor);
    parametres.device_request_length = functor.glength();
    functor.reset();

    functor = std::for_each(consumers_requests.begin(), consumers_requests.end(), functor);
    parametres.consumer_request_length = functor.glength();
}

void Artist::draw()
{
    calculation();
    prepare_machine(buffer_requests, top_buffer, buffer_prepared, top_buffer_num);
    prepare_machine(devices_requests, top_devices, devices_prepared, top_devices_num);
    //prepare_devices();
    prepare_machine(consumers_requests, top_consumers, consumers_prepared, top_consumers_num);
    std::copy(buffer_prepared.begin(), buffer_prepared.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
    std::cout << "\n";
    std::copy(devices_prepared.begin(), devices_prepared.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
    std::cout << "\n";
    std::copy(consumers_prepared.begin(), consumers_prepared.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
    getchar();
    using namespace pdcurses;
    initscr();
    start_color();
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_RED, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    named_rectangle("Devices", origin, point(parametres.device_request_length + 5, devices_prepared.size()*3 + 1), borders::twin_square, COLOR_PAIR(1) | A_BOLD);
    for(unsigned int i = 0; i < devices_prepared.size(); i++)
    {
        std::list< std::string >::iterator start = devices_prepared.begin();
        std::advance(start, i);
        if(i == top_devices_num)
        {
            label((*start).c_str(), point(1, 1 + i*3), point(parametres.device_request_length + 3, (i+1)*3), borders::single_square, COLOR_PAIR(4) | A_BOLD);
        }
        else
        {
            label((*start).c_str(), point(1, 1 + i*3), point(parametres.device_request_length + 3, (i+1)*3), borders::single_square, COLOR_PAIR(2) | A_BOLD);
        }
    }

    named_rectangle("Buffer", point(parametres.device_request_length + 5 + 20,0), point(parametres.device_request_length + parametres.buffer_request_length + 5 + 20, buffer_prepared.size()*3 + 1), borders::twin_square, COLOR_PAIR(1) | A_BOLD);
    for(unsigned int i = 0; i < buffer_prepared.size(); i++)
    {
        std::list< std::string >::iterator start = buffer_prepared.begin();
        std::advance(start, i);
        if(i == top_buffer_num)
        {
            label((*start).c_str(), point(1, 1 + i*3), point(parametres.device_request_length + 3, (i+1)*3), borders::single_square, COLOR_PAIR(4) | A_BOLD);
        }
        else
        {
            label((*start).c_str(), point(1, 1 + i*3), point(parametres.device_request_length + 3, (i+1)*3), borders::single_square, COLOR_PAIR(2) | A_BOLD);
        }
    }
    /*
    pdc::simple_printer printer(0, 0);

    printer = std::for_each(devices_prepared.begin(), devices_prepared.end(), printer);
    printer.reset_xy(parametres.device_request_length + 20, 0);

    printer = std::for_each(buffer_prepared.begin(), buffer_prepared.end(), printer);
    printer.reset_xy(parametres.buffer_request_length + parametres.device_request_length + 40, 0);

    printer = std::for_each(consumers_prepared.begin(), consumers_prepared.end(), printer);

    std::string bind_str(18, '\xCD');
    bind_str[0] = '\xCC';
    bind_str[bind_str.length()- 1] = '\xB9';

    pdcurses::print(bind_str, parametres.device_request_length + 3, 3);
    pdcurses::print(bind_str, parametres.device_request_length + parametres.buffer_request_length + 23, 3);
    */

    getch();
    endwin();
}

/*
void Artist::prepare_machine(const std::list< Request > & request_list, std::list< std::string > & string_list, const std::string name, const unsigned int length)
{
    unsigned int size = request_list.size();
    string_list.push_back(top_machine(length, name));
    for(unsigned int i = 0; i < size; i++)
    {
        std::list< Request >::const_iterator iter = request_list.begin();
        std::advance(iter, i);
        string_list.push_back(request_top(length));
        string_list.push_back(request_mid(length, *iter));
        string_list.push_back(request_bot(length));
    }
    string_list.push_back(bot_machine(length));
}

std::string Artist::top_machine(unsigned int length, const std::string name_)
{
    std::string name(length + 4, '\xCD');
    name[0] = '\xC9';
    name.replace(2, name_.length(), name_);
    name[name.length() - 1] = '\xBB';
    return name;
}

std::string Artist::bot_machine(unsigned int length)
{
    std::string name(length + 4, '\xCD');
    name[0] = '\xC8';
    name[name.length() - 1] = '\xBC';
    return name;
}

std::string Artist::request_top(unsigned int length)
{
    std::string str(length + 4, '\xC4');
    str[0] = '\xBA';
    str[str.length() - 1] = '\xBA';
    str[1] = '\xDA';
    str[str.length() - 2] = '\xBF';
    return str;
}

std::string Artist::request_bot(unsigned int length)
{
    std::string str(length + 4, '\xC4');
    str[0] = '\xBA';
    str[str.length() - 1] = '\xBA';
    str[1] = '\xC0';
    str[str.length() - 2] = '\xD9';
    return str;
}

std::string Artist::request_mid(unsigned int length, const Request & request_)
{
    std::string str(length + 4, ' ');
    str[0] = '\xBA';
    str[1] = '\xB3';
    str[str.length() - 1] = '\xBA';
    str[str.length() - 2] = '\xB3';

    std::stringstream sstream;
    sstream << request_;
    std::string request_out(sstream.str());
    str.replace(2, request_out.length(), request_out);
    return str;
}*/


length_counter::length_counter():
    length(0)
    {

    }

void length_counter::operator()(const Request & request_)
{
    std::stringstream str_stream;
    str_stream << request_;
    if(str_stream.str().size() > length)
    {
        length = str_stream.str().size();
    }
}

void length_counter::reset()
{
    length = 0;
}

unsigned int length_counter::glength() const
{
    return length;
}

void Artist::devices(const Device_union & devices_)
{
    std::vector< Request > dump = devices_.dump();
    devices_requests.clear();
    std::copy(dump.begin(), dump.end(), std::back_inserter(devices_requests));
    if(!devices_.empty())top_devices = devices_.top();
}

void Artist::refused(const Request & request_)
{
    last_refused = request_;
}
