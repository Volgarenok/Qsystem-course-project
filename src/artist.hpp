#ifndef ARTIST_HPP
#define ARTIST_HPP
#include "request.h"
#include "buffer.hpp"
#include "consumer_union.hpp"
#include "device_union.hpp"
#include "geometry/geometry.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <sstream>
#include <list>
#include <algorithm>
#include <iterator>
#include <string>


class request_to_string
{
    public:
        typedef std::string result_type;
        std::string operator()(const Request & request_)
        {
            std::stringstream sstream;
            sstream << request_;
            std::string request_out(sstream.str());
            return request_out;
        }
};

class Artist
{
    public:
        Artist(const Device_union & devices_):
            art_devices(devices_)
            {

            }

        template< class B, class S, class R, class C >
        void buffer(const Buffer< B, S, R, C > & buffer_);

        template< class S >
        void consumers(const Consumer_union< S > & consumers_);

        void devices(const Device_union & devices_);
        void refused(const Request & request_);

        void draw();

    private:
        const Device_union & art_devices;

        Artist(const Artist &);
        Artist & operator= (const Artist &);

        struct draw_parametres
        {
            draw_parametres():
                device_request_length(0),
                buffer_request_length(0),
                consumer_request_length(0)
                {

                }

            unsigned int device_request_length;
            unsigned int buffer_request_length;
            unsigned int consumer_request_length;
        };

        void calculation();

        //std::string top_machine(unsigned int length, const std::string name_);
        //std::string bot_machine(unsigned int length);
        //std::string request_top(unsigned int length);
        //std::string request_bot(unsigned int length);
        //std::string request_mid(unsigned int length, const Request & request_);

        //void prepare_machine(const std::list< Request > & request_list, std::list< std::string > & string_list, const std::string name, const unsigned int length);

        void prepare_machine(const std::list< Request > & request_list, const Request & top_request, std::list< std::string > & string_list, unsigned int & top_num)
        {
            string_list.clear();
            std::transform(request_list.begin(), request_list.end(), std::back_inserter(string_list), boost::bind(request_to_string(), _1));
            if(top_request.valid())
            {
                std::list< Request >::const_iterator found = std::find(request_list.begin(), request_list.end(), top_request);
                assert(found != request_list.end());
                top_num = std::distance(request_list.begin(), found);
            }
            else
            {
                top_num = request_list.size();
            }
        }

        /*
        void prepare_devices()
        {
            devices_requests.clear();
            const std::vector< Request > devices_dump = art_devices.dump();
            std::transform(devices_dump.begin(), devices_dump.end(), std::back_inserter(devices_prepared), boost::bind(request_to_string(), _1));
            if(art_devices.empty())
            {
                top_devices_num = devices_prepared.size();
            }
            else
            {
                std::vector< Request >::const_iterator found = std::find(devices_dump.begin(), devices_dump.end(), art_devices.top());
                assert(found != devices_dump.end());
                top_devices_num = std::distance(devices_dump.begin(), found);
            }
        }*/

        std::list< Request > buffer_requests;
        std::list< Request > consumers_requests;
        std::list< Request > devices_requests;
        Request top_buffer;
        Request top_consumers;
        Request top_devices;

        std::list< std::string > buffer_prepared;
        std::list< std::string > consumers_prepared;
        std::list< std::string > devices_prepared;
        unsigned int top_buffer_num;
        unsigned int top_consumers_num;
        unsigned int top_devices_num;

        Request last_refused;
        draw_parametres parametres;

};

template< class S >
void Artist::consumers(const Consumer_union< S > & consumers_)
{
    std::vector< Request > dump = consumers_.dump();
    consumers_requests.clear();
    std::copy(dump.begin(), dump.end(), std::back_inserter< std::list< Request > >(consumers_requests));
    if(!consumers_.empty()) top_consumers = consumers_.top();
}

template< class B, class S, class R, class C >
void Artist::buffer(const Buffer< B, S, R, C > & buffer_)
{
    std::vector< Request > dump = buffer_.dump();
    buffer_requests.clear();
    std::copy(dump.begin(), dump.end(), std::back_inserter< std::list< Request > >(buffer_requests));
    if(!buffer_.empty()) top_buffer = buffer_.top();
}

class length_counter
{
    public:
        length_counter();
        void operator()(const Request & request_);

        void reset();
        unsigned int glength() const;

    private:
        unsigned int length;
};
#endif // ARTIST_HPP
