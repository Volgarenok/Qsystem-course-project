#ifndef DISTRIBUTIONS_H
#define DISTRIBUTIONS_H
#include "distributions/exponential_distribution.h"
#include "distributions/normal_distribution.h"
#include "distributions/uniform_distribution.h"
#include "distributions/simple_distribution.h"
#endif // DISTRIBUTIONS_H
