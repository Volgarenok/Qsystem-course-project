#define BOOST_TEST_MODULE Test request
#include "request.h"
#include <boost/test/included/unit_test.hpp>
#include <boost/bind.hpp>
#include <vector>
#include <iterator>
#include <algorithm>
#include <iostream>

BOOST_AUTO_TEST_CASE(test_construct_and_compare)
{
    Request request_1 = Request(1, 0.2, 1);
    Request request_2 = Request();
    Request request_3 = Request();
    Request request_4 = Request(1, 0.2, 1);

    BOOST_TEST(request_1.valid());
    BOOST_TEST(!request_2.valid());
    BOOST_TEST(request_1 == request_4);
    BOOST_TEST(request_2 == request_3);
    BOOST_TEST(request_1 != request_2);
    request_1.make_invalid();
    BOOST_TEST(request_1 != request_4);
}

BOOST_AUTO_TEST_CASE(test_output_and_geters)
{
    Request request = Request(1, 0.2);
    std::vector< Request > storage(1, request);
    std::copy(storage.begin(), storage.end(), std::ostream_iterator< Request >(std::cout, "\n"));
}
/*
BOOST_AUTO_TEST_CASE(test_compare_birth_1)
{
    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_5 = Request(4, 2.0, 4);
    request_3.make_invalid();

    std::vector< Request > test_storage;
    std::vector< Request > control_storage;

    test_storage.push_back(request_1);
    test_storage.push_back(request_5);
    test_storage.push_back(request_3);
    test_storage.push_back(request_2);

    control_storage.push_back(request_1);
    control_storage.push_back(request_2);
    control_storage.push_back(request_5);
    control_storage.push_back(request_3);

    std::sort(test_storage.begin(), test_storage.end(), boost::bind(ascend_birth_compare(), _1, _2));

    BOOST_TEST(test_storage == control_storage);
}

BOOST_AUTO_TEST_CASE(test_compare_birth_2)
{
    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_5 = Request(4, 2.0, 4);
    request_3.make_invalid();

    std::vector< Request > test_storage;
    std::vector< Request > control_storage;

    test_storage.push_back(request_1);
    test_storage.push_back(request_5);
    test_storage.push_back(request_3);
    test_storage.push_back(request_2);

    control_storage.push_back(request_5);
    control_storage.push_back(request_2);
    control_storage.push_back(request_1);
    control_storage.push_back(request_3);

    std::sort(test_storage.begin(), test_storage.end(), boost::bind(descend_birth_compare(), _1, _2));

    BOOST_TEST(test_storage == control_storage);
}

BOOST_AUTO_TEST_CASE(test_compare_priority_1)
{
    std::vector< Request > storage_3;
    std::vector< Request > priority_check;

    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_5 = Request(4, 2.0, 4);
    request_5.make_invalid();

    storage_3.push_back(request_5);
    storage_3.push_back(request_2);
    storage_3.push_back(request_1);
    storage_3.push_back(request_3);

    priority_check.push_back(request_1);
    priority_check.push_back(request_2);
    priority_check.push_back(request_3);
    priority_check.push_back(request_5);

    std::sort(storage_3.begin(), storage_3.end(), boost::bind(descend_priority_compare(), _1, _2));

    BOOST_TEST(storage_3 == priority_check);
}

BOOST_AUTO_TEST_CASE(test_compare_priority_2)
{
    std::vector< Request > storage_3;
    std::vector< Request > priority_check;

    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_5 = Request(4, 2.0, 4);
    request_5.make_invalid();

    storage_3.push_back(request_5);
    storage_3.push_back(request_2);
    storage_3.push_back(request_1);
    storage_3.push_back(request_3);

    priority_check.push_back(request_3);
    priority_check.push_back(request_2);
    priority_check.push_back(request_1);
    priority_check.push_back(request_5);

    std::sort(storage_3.begin(), storage_3.end(), boost::bind(ascend_priority_compare(), _1, _2));

    BOOST_TEST(storage_3 == priority_check);
}

BOOST_AUTO_TEST_CASE(test_is_executed)
{
    Request request_1;
    BOOST_TEST(!request_1.is_executed());
    Request request_2 = Request(2, 0.5, 2);
    BOOST_TEST(!request_2.is_executed());
    request_2.execute(2.3d);
    BOOST_TEST(request_2.is_executed());
}


namespace
{
        struct Priority_comporator
        {
            typedef bool result_type;
            result_type operator()(const Request & lhs, const Request & rhs)
            {
                bool a = lhs.valid();
                bool b = rhs.valid();
                bool c = lhs.priority() < rhs.priority();
                return a*b*c ||!a*!b*c;
            }
        };

        struct Valid_comporator
        {
            typedef bool result_type;
            result_type operator()(const Request & lhs, const Request & rhs)
            {
                return lhs.valid() > rhs.valid();
            }
        };

        struct Test_comporator
        {
            typedef bool result_type;
            result_type operator()(Request & lhs, Request & rhs)
            {
                if(rhs.valid() > lhs.valid())
                {
                    //std::swap(rhs, lhs);
                }
                return lhs.priority() * rhs.valid() > rhs.priority() * lhs.valid();
            }
        };
}

BOOST_AUTO_TEST_CASE(priority_compare_test)
{
    std::vector< Request > storage_3;
    std::vector< Request > priority_check;

    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_4 = Request(5, 0.7, 0);
    Request request_5 = Request(4, 2.0, 4);
    request_5.make_invalid();
    request_4.make_invalid();

    storage_3.push_back(request_5);
    storage_3.push_back(request_2);
    storage_3.push_back(request_4);
    storage_3.push_back(request_1);
    storage_3.push_back(request_3);

    priority_check.push_back(request_1);
    priority_check.push_back(request_2);
    priority_check.push_back(request_3);
    priority_check.push_back(request_4);
    priority_check.push_back(request_5);

    std::sort(storage_3.begin(), storage_3.end(), boost::bind(Valid_comporator(), _1, _2));
    std::sort(storage_3.begin(), storage_3.end(), boost::bind(Priority_comporator(), _1, _2));
    BOOST_TEST(storage_3 == priority_check);
}


BOOST_AUTO_TEST_CASE(priority_compare_test_2)
{
    std::vector< Request > storage_3;
    std::vector< Request > priority_check;

    Request request_1 = Request(1, 0.1, 1);
    Request request_2 = Request(2, 0.5, 2);
    Request request_3 = Request(3, 1.0, 3);
    Request request_4 = Request(5, 0.7, 0);
    Request request_5 = Request(4, 2.0, 4);
    request_5.make_invalid();
    request_4.make_invalid();

    storage_3.push_back(request_5);
    storage_3.push_back(request_2);
    storage_3.push_back(request_4);
    storage_3.push_back(request_1);
    storage_3.push_back(request_3);

    priority_check.push_back(request_3);
    priority_check.push_back(request_2);
    priority_check.push_back(request_1);
    priority_check.push_back(request_5);
    priority_check.push_back(request_4);

    std::sort(storage_3.begin(), storage_3.end(), boost::bind(Valid_comporator(), _1, _2));
    std::sort(storage_3.begin(), storage_3.end(), boost::bind(Priority_comporator(), _2, _1));
    BOOST_TEST(storage_3 == priority_check);
*/
