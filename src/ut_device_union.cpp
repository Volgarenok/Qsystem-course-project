#define BOOST_TEST_MODULE Test device union
#include "device.hpp"
#include "device_union.hpp"
#include "request.h"
#include "distributions.h"
#include <boost/test/included/unit_test.hpp>
#include <boost/bind.hpp>
#include <cstdlib>
#include <iostream>
#include <list>

BOOST_AUTO_TEST_CASE(test_device_union_output)
{
    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));

    Device_union devices;
    devices.push_device(u_device);
    devices.push_device(e_device);
    devices.push_device(n_device);

    std::cout << devices << std::endl;
    std::cout << u_device.top() << std::endl << e_device.top() << std::endl << n_device.top() << std::endl;
}

BOOST_AUTO_TEST_CASE(test_device_union_pop_and_push_device)
{
    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));

    Device_union devices;
    devices.push_device(u_device);
    devices.push_device(e_device);
    devices.push_device(n_device);

    BOOST_TEST(devices.size() == 3);
    devices.pop_device(1);
    devices.pop_device(2);

    BOOST_TEST(!devices.in_union(5));
    devices.pop_device(5);

    devices.push_device(n_device);
    BOOST_TEST(devices.in_union(3));
    devices.pop_device(3);

    BOOST_TEST(devices.empty());
}

namespace
{
    class contain_in
    {
        public:
            typedef bool result_type;

            contain_in(const std::list< Request > & requests_):
                main_check(true),
                requests(requests_)
                {

                }

            bool operator()(const Request request)
            {
                std::list< Request >::iterator found = std::find(requests.begin(), requests.end(), request);
                bool check = (found != requests.end());
                main_check &= check;
                return main_check;
            }

            bool check_result()
            {
                return main_check;
            }

        private:
            bool main_check;
            std::list< Request > requests;
    };
}

BOOST_AUTO_TEST_CASE(test_top_pop_requests)
{
    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));

    Device_union devices;
    devices.push_device(u_device);
    devices.push_device(e_device);
    devices.push_device(n_device);

    std::list< Request > union_list;
    for(unsigned int i = 0; i < 10; i++)
    {
        union_list.push_back(devices.top());
        devices.pop();
    }

    std::list< Request > device_list;
    device_list.push_back(u_device.top());
    device_list.push_back(e_device.top());
    device_list.push_back(n_device.top());

    contain_in functor_1(union_list);
    std::for_each(device_list.begin(), device_list.end(), boost::bind(functor_1, _1));
    BOOST_TEST(functor_1.check_result());
}
