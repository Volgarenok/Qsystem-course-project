#ifndef DEVICE_HPP
#define DEVICE_HPP
#include "request.h"
#include "io/io_state_guard.h"
#include "device_base.hpp"
#include <iomanip>
#include <iostream>

template < typename Time_distribution >
class Device: public Device_base
{
    public:
        Device(const unsigned int number_, const Time_distribution distribution_, const unsigned int priority_ = 0);
        virtual ~Device();

        const Request & top() const;
        void pop();
        void reset();

        unsigned int amount() const;
        unsigned int number() const;
        unsigned int priority() const;
        Time_distribution distribution() const;

    private:
        Device();

        Time_distribution t_distribution;

        Request current;

        double last_time;

        unsigned int device_priority;
        unsigned int amount_requests;
        unsigned int device_number;
};

template < typename Time_distribution >
std::ostream & operator<< (std::ostream & out, const Device< Time_distribution > & device_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
		out << "(:priority " << std::setw(1) << device_.priority() << " ";
		out << ":amount "  << std::setw(3) << device_.amount() << " ";
        out << ":number "  << std::setw(1) << device_.number() <<  " ";
        out << ":distribution "  << std::setw(3) << device_.distribution() << ")";
    }
    return out;
}


template < typename Time_distribution >
Device< Time_distribution >::~Device()
{

}

template < typename Time_distribution >
Device< Time_distribution >::Device(const unsigned int number_, const Time_distribution distribution_, const unsigned int priority_):
    t_distribution(distribution_),
    current(),
    last_time(0.0),
    device_priority(priority_),
    amount_requests(0),
    device_number(number_)
    {
        pop();
    }

template < typename Time_distribution >
const Request & Device< Time_distribution >::top() const
{
    return current;
}

template < typename Time_distribution >
void Device< Time_distribution >::pop()
{
    last_time = last_time + t_distribution.get();
    current = Request(device_number, last_time, device_priority);
    amount_requests++;
}

template < typename Time_distribution >
void Device< Time_distribution >::reset()
{
    current = Request();
    last_time = 0.0d;
    amount_requests = 0u;
    pop();
}

template < typename Time_distribution >
unsigned int Device< Time_distribution >::priority() const
{
    return device_priority;
}

template < typename Time_distribution >
unsigned int Device< Time_distribution >::amount() const
{
    return amount_requests;
}

template < typename Time_distribution >
unsigned int Device< Time_distribution >::number() const
{
    return device_number;
}

template < typename Time_distribution >
Time_distribution Device< Time_distribution >::distribution() const
{
    return t_distribution;
}
#endif // DEVICE_HPP
