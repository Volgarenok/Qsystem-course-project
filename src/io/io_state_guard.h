#ifndef IO_STATE_GUARD_H
#define IO_STATE_GUARD_H
#include <ostream>
namespace
{
	class io_state_guard
	{
		public:
			io_state_guard(std::ostream & stream):
				stream_ref(stream),
				fill_char(stream.fill()),
				format_flags(stream.flags())
				{
				}

			~io_state_guard()
			{
				stream_ref.fill(fill_char);
				stream_ref.flags(format_flags);
			}
		
		private:
			std::ostream & stream_ref;
			std::ostream::char_type fill_char;
			std::ostream::fmtflags format_flags;
			
			io_state_guard(const io_state_guard &);
			io_state_guard & operator= (const io_state_guard &);
	};
}
#endif
