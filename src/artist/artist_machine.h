#ifndef ARTIST_MACHINE_H
#define ARTIST_MACHINE_H
#include "../geometry/types.hpp"
#include "artist_request.h"
#include <boost/bind.hpp>
#include <algorithm>
#include <vector>
class AMachine
{
    public:
        typedef geometry::string_type string_type;
        typedef geometry::point point_type;

        template < class Machine >
        AMachine(const Machine & machine, const char * name, const geometry::point & pt_, const unsigned int attribute_1_ = 0, const unsigned int attribute_2_ = 0, const unsigned int attribute_3_ = 0);
        AMachine(const Request & request, const char * name, const geometry::point & pt_, const unsigned int attribute_1_ = 0, const unsigned int attribute_2_ = 0, const unsigned int attribute_3_ = 0);

        unsigned int size() const;
        unsigned int glength() const;
        void draw() const;
        void setpt(const point_type & pt_);

    private:
        class length_counter
        {
            public:
                length_counter();
                void operator()(const ARequest & arequest_);

                void reset();
                unsigned int glength() const;

            private:
                unsigned int length;
        };

        class rtransformer
        {
            public:
                typedef ARequest result_type;
                rtransformer(const point_type pt_, const Request & top, const unsigned int top_attr, const unsigned int other_attr, const unsigned int invalid_attr);
                const result_type operator()(const Request & request);

            private:
                rtransformer();

                Request top_request;
                unsigned int top_attribute;
                unsigned int other_attribute;
                unsigned int invalid_attribute;
                point_type pt;
                unsigned int counter;
        };

        unsigned int top_attribute;
        unsigned int other_attribute;
        unsigned int invalid_attribute;
        string_type machine_name;
        unsigned int amount;
        unsigned int request_length;
        point_type pt;
        std::vector< ARequest > requests;
};

template < class Machine >
AMachine::AMachine(const Machine & machine, const char * name, const point_type & pt_, const unsigned int attribute_1_, const unsigned int attribute_2_, const unsigned int attribute_3_):
    top_attribute(attribute_1_),
    other_attribute(attribute_2_),
    invalid_attribute(attribute_3_),
    machine_name(name),
    amount(machine.size()),
    request_length(0),
    pt(pt_),
    requests()
    {
        Request top_request;
        if(!machine.empty())
        {
            top_request = machine.top();
        }

        const typename Machine::dump_type machine_dump(machine.dump());
        std::transform(machine_dump.begin(), machine_dump.end(), std::back_inserter(requests), boost::bind(rtransformer(pt, top_request, top_attribute, other_attribute, invalid_attribute), _1));
        const length_counter lc = std::for_each(requests.begin(), requests.end(), length_counter());
        request_length = lc.glength();
    }
#endif // ARTIST_MACHINE_H
