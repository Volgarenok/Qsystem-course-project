#ifndef ARTIST_REQUEST_H
#define ARTIST_REQUEST_H
#include "../request.h"
#include "../geometry/types.hpp"
class request_to_string
{
    public:
        typedef geometry::string_type result_type;
        result_type operator()(const Request & request_) const;
};

class ARequest
{
    public:
        typedef geometry::point point_type;
        typedef geometry::string_type string_type;

        ARequest(const Request & request_, const point_type & pt_, const unsigned int attribute_ = 0);
        unsigned int length() const;
        void draw() const;
        void draw(const unsigned int length) const;
        void setpt(const point_type & pt_);

    private:
        point_type pt;
        string_type str;
        unsigned int attribute;
};
#endif // ARTIST_REQUEST_H
