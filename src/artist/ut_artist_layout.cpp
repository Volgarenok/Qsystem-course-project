#define BOOST_TEST_MODULE Test machine artist
#include "artist_machine.h"
#include "../distributions.h"
#include "../device_union.hpp"
#include "../device.hpp"
#include "../buffer.hpp"
#include "../consumer.hpp"
#include "../consumer_union.hpp"
#include "../disciplines.h"
#include "artist_layout.hpp"
#include "curses.h"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(artist_device_test)
{
    Device< Uniform_distribution > u_device(1, Uniform_distribution(1.0, 3.0));
    Device< Exponential_distribution > e_device(2, Exponential_distribution());
    Device< Normal_distribution > n_device(3, Normal_distribution(5.0));
    Device_union devices;
    devices.push_device(u_device);
    devices.push_device(e_device);
    devices.push_device(n_device);

    Consumer< Uniform_distribution > u_consumer(1, Uniform_distribution(1.0, 3.0));
    Consumer< Exponential_distribution > e_consumer(2, Exponential_distribution());
    Consumer< Normal_distribution > n_consumer(3, Normal_distribution(5.0));
    Consumer_union< Selectable > consumers;
    consumers.push_consumer(u_consumer);
    consumers.push_consumer(e_consumer);
    consumers.push_consumer(n_consumer);

    Buffer<> buffer(2);
    Request request_1(1, 2.0);
    buffer.push(request_1);

    Request request_2(1, 3.0);


    geometry::init_std_curses();

    ALayout layout = ALayout(buffer, devices, consumers, request_1, request_2, COLOR_PAIR(1) | A_BOLD, COLOR_PAIR(2) | A_BOLD, A_BOLD);
    layout.draw();

    getch();
    geometry::end_curses();
}
