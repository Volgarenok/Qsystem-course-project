#include "artist_machine.h"
#include "../geometry/geometry.hpp"
AMachine::length_counter::length_counter():
    length(0)
    {

    }

void AMachine::length_counter::operator()(const ARequest & arequest_)
{
    if(arequest_.length() > length)
    {
        length = arequest_.length();
    }
}

void AMachine::length_counter::reset()
{
    length = 0;
}

unsigned int AMachine::length_counter::glength() const
{
    return length;
}

AMachine::rtransformer::rtransformer(const point_type pt_, const Request & top, const unsigned int top_attr, const unsigned int other_attr, const unsigned int invalid_attr):
    top_request(top),
    top_attribute(top_attr),
    other_attribute(other_attr),
    invalid_attribute(invalid_attr),
    pt(pt_),
    counter(0)
    {
    }

const AMachine::rtransformer::result_type AMachine::rtransformer::operator()(const Request & request)
{
    if(top_request.valid() && request == top_request)
    {
        return ARequest(request, pt.move(1, 1 + (counter++)*3), top_attribute);
    }
    else
    {
        if(request.valid())
        {
            return ARequest(request, pt.move(1, 1 + (counter++)*3), other_attribute);
        }
        else
        {
            return ARequest(request, pt.move(1, 1 + (counter++)*3), invalid_attribute);
        }
    }
}

AMachine::AMachine(const Request & request, const char * name, const geometry::point & pt_, const unsigned int attribute_1_, const unsigned int attribute_2_, const unsigned int attribute_3_):
    top_attribute(attribute_1_),
    other_attribute(attribute_2_),
    invalid_attribute(attribute_3_),
    machine_name(name),
    amount(1),
    request_length(0),
    pt(pt_),
    requests()
    {
        const std::vector< Request > machine_dump(1, request);
        std::transform(machine_dump.begin(), machine_dump.end(), std::back_inserter(requests), boost::bind(rtransformer(pt, request, top_attribute, other_attribute, invalid_attribute), _1));
        const length_counter lc = std::for_each(requests.begin(), requests.end(), length_counter());
        request_length = lc.glength();
    }

namespace
{
    class drawer
    {
        public:
            typedef void result_type;
            drawer(const unsigned int length_):
                length(length_)
                {
                }

            result_type operator()(const ARequest & arequest_)
            {
                arequest_.draw(length);
                return;
            }

        private:
            unsigned int length;
    };
}

void AMachine::draw() const
{
    geometry::named_rectangle(machine_name.c_str(), pt, pt.move(request_length + 3, amount * 3 + 1), geometry::borders::twin_square, other_attribute);
    std::for_each(requests.begin(), requests.end(), boost::bind(drawer(request_length), _1));
}

unsigned int AMachine::size() const
{
    return amount;
}

unsigned int AMachine::glength() const
{
    return request_length;
}

void AMachine::setpt(const point_type & pt_)
{
    pt = pt_;
    for(unsigned int i = 0; i < requests.size(); i++)
    {
        requests[i].setpt(pt_.move(1, 1 + i*3));
    }
}
