#include "artist_request.h"
#include "../geometry/geometry.hpp"
#include <sstream>
#include <cassert>
request_to_string::result_type request_to_string::operator()(const Request & request_) const
{
    std::stringstream sstream;
    sstream << request_;
    return result_type(sstream.str());
}

namespace
{
    const request_to_string rts;
}

ARequest::ARequest(const Request & request_, const point_type & pt_, const unsigned int attribute_):
    pt(pt_),
    str(rts.operator()(request_)),
    attribute(attribute_)
    {

    }

void ARequest::draw() const
{
    geometry::label(str.c_str(), pt, pt.move(str.length() + 1, 2), geometry::borders::single_square, attribute);
}

void ARequest::draw(const unsigned int length) const
{
    assert(length >= str.length());
    geometry::label(str.c_str(), pt, pt.move(length + 1, 2), geometry::borders::single_square, attribute);
}

unsigned int ARequest::length() const
{
    return str.length();
}

void ARequest::setpt(const point_type & pt_)
{
    pt = pt_;
}
