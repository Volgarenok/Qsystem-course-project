#define BOOST_TEST_MODULE Test request artist
#include "artist_request.h"
#include "curses.h"
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(artist_request_test)
{
    Request request(1, 2.0, 3);

    geometry::init_std_curses();
    ARequest a_req(request, geometry::point(geometry::origin), COLOR_PAIR(4) | A_BOLD);
    a_req.draw();
    a_req.setpt(geometry::origin.move(20, 20));
    a_req.draw();
    getch();
    geometry::end_curses();
}
