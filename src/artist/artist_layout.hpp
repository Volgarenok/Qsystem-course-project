#ifndef ARTIST_LAYOUT_HPP
#define ARTIST_LAYOUT_HPP
#include "artist_machine.h"
#include <vector>
#include <utility>
class ALayout
{
    public:
        template < class B, class D, class C >
        ALayout(const B & buffer, const D & devices, const C & consumers, const Request & last_refused, const Request & last_consumed, const unsigned int attr_1 = 0, const unsigned int attr_2 = 0, const unsigned int attr_3 = 0):
            bind_1(),
            bind_2(),
            bind_3(),
            bind_4(),
            main_attribute(attr_2),
            machines()
            {
                machines.push_back(AMachine(devices, "Devices", geometry::origin, attr_1, attr_2, attr_3));
                machines.push_back(AMachine(buffer, "Buffer", geometry::origin, attr_1, attr_2, attr_3));
                machines.push_back(AMachine(consumers, "Consumers", geometry::origin, attr_1, attr_2, attr_3));
                machines.push_back(AMachine(last_refused, "Refused", geometry::origin, attr_1, attr_2, attr_3));
                machines.push_back(AMachine(last_consumed, "Consumed", geometry::origin, attr_1, attr_2, attr_3));
                calculate_layout();
            }

        void draw() const;

    private:
        void calculate_layout();

        std::pair< geometry::point, geometry::point > bind_1;
        std::pair< geometry::point, geometry::point > bind_2;
        std::pair< geometry::point, geometry::point > bind_3;
        std::pair< geometry::point, geometry::point > bind_4;

        unsigned int main_attribute;
        std::vector< AMachine > machines;
};
#endif // ARTIST_LAYOUT_HPP
