#include "artist_layout.hpp"
#include "../geometry/geometry.hpp"
#include <algorithm>
#include <cassert>
namespace
{
    class drawer
    {
        public:
            typedef void result_type;
            drawer()
                {
                }

            result_type operator()(const AMachine & amachine_)
            {
                amachine_.draw();
                return;
            }
    };
}

void ALayout::draw() const
{
    std::for_each(machines.begin(), machines.end(), boost::bind(drawer(), _1));
    geometry::snippet_h(bind_1.first, bind_1.second, geometry::tips::twin_h, geometry::chars::twin_h, main_attribute);
    geometry::snippet_h(bind_2.first, bind_2.second, geometry::tips::twin_h, geometry::chars::twin_h, main_attribute);
    geometry::snippet_v(bind_3.first, bind_3.second, geometry::tips::twin_v, geometry::chars::twin_v, main_attribute);
    geometry::snippet_v(bind_4.first, bind_4.second, geometry::tips::twin_v, geometry::chars::twin_v, main_attribute);
}

void ALayout::calculate_layout()
{
    machines[1].setpt(geometry::origin.move(machines[0].glength() + 15, 0));
    machines[2].setpt(geometry::origin.move(machines[0].glength() + 30 + machines[1].glength(), 0));
    machines[3].setpt(geometry::origin.move(machines[0].glength() + 15, machines[1].size() * 3 + 2 + 15));
    machines[4].setpt(geometry::origin.move(machines[0].glength() + 30 + machines[1].glength(), machines[2].size() * 3 + 2 + 15));

    bind_1.first = geometry::point(machines[0].glength() + 3, 2);
    bind_1.second = geometry::point(machines[0].glength() + 15, 2);

    bind_2.first = geometry::point(machines[0].glength() + 18 + machines[1].glength(), 2);
    bind_2.second = geometry::point(machines[0].glength() + 30 + machines[1].glength(), 2);

    bind_3.first = geometry::point(machines[0].glength() + 25, machines[1].size() * 3 + 1);
    bind_3.second = geometry::point(machines[0].glength() + 25 , machines[1].size() * 3 + 2 + 15);

    bind_4.first = geometry::point(machines[0].glength() + 40 + machines[1].glength(), machines[2].size() * 3 + 1);
    bind_4.second = geometry::point(machines[0].glength() + 40 + machines[1].glength(), machines[2].size() * 3 + 2 + 15);
}
