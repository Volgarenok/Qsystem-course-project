#ifndef REQUEST_H
#define REQUEST_H
#include <ostream>
class Request
{
    public:
        Request();
        Request(const unsigned int source_, const double time_, const unsigned int priority_ = 1);
        bool operator==(const Request& rhs) const;
        bool operator!=(const Request& rhs) const;
        bool valid() const;
        bool is_executed() const;
        void execute(const unsigned int executioner_, const double time_, const unsigned int executioner_priority_ = 0);
        void await(const double time_);
        void make_invalid();
        double execution() const;
        double idle() const;
        double birth() const;
        unsigned int priority() const;
        unsigned int epriority() const;
        unsigned int source() const;
        unsigned int executioner() const;

        void force_priority();
        void force_epriority();

    private:
        unsigned int source_number;
        unsigned int executioner_number;
        unsigned int request_priority;
        unsigned int executioner_priority;

        double birth_time;
        double wait_time;
        double exec_time;

        bool validity;
};

std::ostream & operator<< (std::ostream & out, const Request & request_);

struct is_valid
{
    typedef bool result_type;
    result_type operator()(const Request& request_) const;
};

struct is_invalid
{
    typedef bool result_type;
    result_type operator()(const Request& request_) const;
};

/*
struct ascend_priority_compare
{
    typedef bool result_type;
    result_type operator()(const Request& lhs, const Request& rhs) const;
};
struct descend_priority_compare
{
    typedef bool result_type;
    result_type operator()(const Request& lhs, const Request& rhs) const;
};

struct descend_birth_compare
{
    typedef bool result_type;
    result_type operator()(const Request& lhs, const Request& rhs) const;
};

struct ascend_birth_compare
{
    typedef bool result_type;
    result_type operator()(const Request& lhs, const Request& rhs) const;
};*/

/*! \class idle generator functor
*   \brief use for generate idle in request sequences
*/
class idle_by
{
    public:
        typedef void result_type;
        idle_by(const Request & request_):
            time_point(request_.execution())
            {
            }

        void operator()(Request & request_)
        {
            if(time_point - request_.birth() > 0)
            {
                request_.await(time_point);
            }
        }

    private:
        double time_point;
};
#endif // REQUEST_H
