#include "src/request.h"
#include "artist.hpp"
#include "src/disciplines.h"
#include <iostream>

int main(int, char**)
{
    Artist artist;

    Request req_1(1, 2.0, 2);
    Request req_2(2, 1.0, 3);
    req_2.await(1.7);
    Request req_3(1, 0.3, 1);
    req_3.await(0.9);
    req_3.execute(0.8, 2);

    artist.refused(req_2);
    artist.draw();
    std::cout << "|" << req_1 << "|" << req_2 << "|" << req_3 << "|" << std::endl;
    return 0;
}
