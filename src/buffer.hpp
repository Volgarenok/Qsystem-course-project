#ifndef BUFFER_TMP_HPP
#define BUFFER_TMP_HPP
#include "request.h"
#include "disciplines.h"
#include "io/io_state_guard.h"
#include <algorithm>
#include <memory>
#include <vector>
#include <ostream>
#include <iterator>
template < class Bufferable = Free, class Selectable = Ordinal, class Removable = Incoming, template <class, class> class Container = std::vector >
class Buffer
{
    public:
        typedef Container< Request, std::allocator< Request > > container_type;
        typedef Container< Request, std::allocator< Request > > dump_type;
        typedef typename container_type::const_iterator const_iter_type;
        typedef typename container_type::iterator iter_type;

        Buffer(const unsigned int size_ = 1);
        Buffer(const container_type & init_container);

        unsigned int size() const;
        bool full() const;
        bool empty() const;
        template < class B, class S, class R, template <class, class> class C >
        bool operator==(const Buffer< B, S, R, C >& rhs) const
        {
            return std::equal(this->storage.begin(), this->storage.end(), rhs.dump().begin());
        }

        template < class B, class S, class R, template <class, class> class C >
        bool operator!=(const Buffer< B, S, R, C >& rhs) const
        {
            return !std::equal(this->storage.begin(), this->storage.end(), rhs.dump().begin());
        }

        const Request & top() const;

        const Request push(const Request & request_);
        void pop();

        void await(const Request & request_);

        const dump_type & dump() const;

    private:
        const_iter_type find(const Request & request_) const;
        iter_type find(const Request & request_);

        Bufferable buffer_dscpl;
        Selectable select_dscpl;
        Removable remove_dscpl;

        unsigned int current_amount;
        container_type storage;
};

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
std::ostream & operator<< (std::ostream & out, const Buffer< Bufferable, Selectable, Removable, Container > & buffer_)
{
    std::ostream::sentry sentry(out);
    if(sentry)
    {
        ::io_state_guard state(out);
        out << "(:size " << buffer_.size() << " ";
        out << ":dump \n";
        out << "\t{\n";
        out << "\t\t";
        std::copy(buffer_.dump().begin(), buffer_.dump().end(), std::ostream_iterator< Request >(out, "\n\t\t"));
        out << "\b\b\b\b\b\b\b\b}\n" << ")";
    }
    return out;
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
Buffer< Bufferable, Selectable, Removable, Container >::Buffer(const unsigned int size_):
    buffer_dscpl(),
    select_dscpl(),
    remove_dscpl(),
    current_amount(0u),
    storage(size_)
    {
    }

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
Buffer< Bufferable, Selectable, Removable, Container >::Buffer(const container_type & init_container):
    buffer_dscpl(),
    select_dscpl(),
    remove_dscpl(),
    current_amount(std::count_if(init_container.begin(), init_container.end(), is_valid())),
    storage(init_container)
    {
        buffer_dscpl.update(storage);
    }

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
unsigned int Buffer< Bufferable, Selectable, Removable, Container >::size() const
{
    return storage.size();
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
bool Buffer< Bufferable, Selectable, Removable, Container >::full() const
{
    return current_amount == storage.size();
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
bool Buffer< Bufferable, Selectable, Removable, Container >::empty() const
{
    return current_amount == 0u;
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
const typename Buffer< Bufferable, Selectable, Removable, Container >::dump_type & Buffer< Bufferable, Selectable, Removable, Container >::dump() const
{
    return storage;
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
const Request & Buffer< Bufferable, Selectable, Removable, Container >::top() const
{
    assert(!empty());
    //Request returnable = select_dscpl.select(storage, buffer_dscpl.pointer());
    Request returnable = select_dscpl.select(storage, is_valid(), buffer_dscpl.pointer());
    const_iter_type iter = find(returnable);
    return *iter;
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
const Request Buffer< Bufferable, Selectable, Removable, Container >::push(const Request & request_)
{
    Request returnable;
    if(full())
    {
        Request removable = remove_dscpl.remove(storage, buffer_dscpl.pointer(), request_);
        iter_type iter = find(removable);
        if(iter != storage.end())
        {
            returnable = *iter;
            iter->make_invalid();
            buffer_dscpl.update(storage);
        }
        else
        {
            returnable = request_;
            return returnable;
        }
    }
    buffer_dscpl.emplace(storage, request_);
    buffer_dscpl.update(storage);
    if(!returnable.valid())
    {
        current_amount++;
    }
    return returnable;
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
void Buffer< Bufferable, Selectable, Removable, Container >::pop()
{
    assert(!empty());
    //const Request returnable = select_dscpl.select(storage, buffer_dscpl.pointer());
    const Request returnable = select_dscpl.select(storage, is_valid(), buffer_dscpl.pointer());
    iter_type iter = find(returnable);
    iter->make_invalid();
    buffer_dscpl.update(storage);
    current_amount--;
    return;
}


template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
void Buffer< Bufferable, Selectable, Removable, Container >::await(const Request & request_)
{
    std::for_each(storage.begin(), storage.end(), idle_by(request_));
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
typename Buffer< Bufferable, Selectable, Removable, Container >::const_iter_type Buffer< Bufferable, Selectable, Removable, Container >::find(const Request & request_) const
{
    return std::find(storage.begin(), storage.end(), request_);
}

template < class Bufferable, class Selectable, class Removable, template <class, class> class Container >
typename Buffer< Bufferable, Selectable, Removable, Container >::iter_type Buffer< Bufferable, Selectable, Removable, Container >::find(const Request & request_)
{
    return std::find(storage.begin(), storage.end(), request_);
}
#endif // BUFFER_TMP_HPP
